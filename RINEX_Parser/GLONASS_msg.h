#pragma once
#include "RINEX_Message.h"
#include "Message_factory.h"

namespace rinex
{
	struct GLONASS_msg_data
	{
		std::uint8_t sv_id;
		std::uint16_t year;
		std::uint8_t month;
		std::uint8_t day;
		std::uint8_t hour;
		std::uint8_t minute;
		std::uint8_t second;
		double x;
		double y;
		double z;
		double v_x;
		double v_y;
		double v_z;
	};

	class GLONASS_msg : public RINEX_Message
	{
		std::shared_ptr<GLONASS_msg_data> parsed_data;

	
	public:
		GLONASS_msg(RINEX_Msg_Id msg_id,
					std::vector<std::uint8_t>&& data);

		void do_parse() override;

		std::shared_ptr<igsk_data> to_igsk() override;
	};

	class GLONASS_msg_factory : public Message_factory
	{
		std::shared_ptr<RINEX_Message> create(RINEX_Msg_Id msg_id,
						std::vector<std::uint8_t>&& data) override;
	};
}