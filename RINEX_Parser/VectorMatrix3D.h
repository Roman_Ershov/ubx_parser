//
// Created by yelowt on 24.10.17.
//

#ifndef BNO_VECTORMATRIX_H
#define BNO_VECTORMATRIX_H

struct Vector3D;

/** Основные типы поворотов.*/
enum TURN_TYPE
{
    /** Поворот относительно оси X.*/
            TURN_X,

    /** Поворот относительно оси Y.*/
            TURN_Y,

    /** Поворот относительно оси Z.*/
            TURN_Z
};

/** Трехмерная матрица 3x3.*/
struct Matrix3D
{
    /** Компоненты трехмерной матрицы 3х3.*/
    double value[3*3];

    /** Конструктор по-умолчанию.*/
    Matrix3D();

    /** Конструктор с параметрами.
        @param type  тип поворота.
        @param angle угол поворота, радианы.*/
    Matrix3D(TURN_TYPE type, double angle);

    /** Конструктор с параметрами.
        @param type  тип поворота.
        @param s синус угла поворота.
        @param c косинус угла поворота.*/
    Matrix3D(TURN_TYPE type, double s, double c);

    /** Построить матрицу поворота.
        @param type  тип поворота.
        @param angle угол поворота, радианы.*/
    void CreateTurnMatrix(TURN_TYPE type, double angle);

    /** Построить матрицу поворота.
        @param type  тип поворота.
        @param s синус угла поворота.
        @param c косинус угла поворота.*/
    void CreateTurnMatrix(TURN_TYPE type, double s, double c);

    /** Транспонировать матрицу*/
    void ToTranspose();


    /** Получить транспонированную матрицу*/
    Matrix3D operator-();

    /** Получть результат умножения вектора на матрицу.
        @param v вектор (строка).
        @param tm матрица.
        @return вектор r = v*tm.*/
    friend const Vector3D operator*(const Vector3D& v, const Matrix3D& tm);

    /** Получть результат умножения матрицы на вектор.
        @param tm матрица.
        @param v вектор (столбец).
        @return вектор r = v * tm.*/
    friend const Vector3D operator*(const Matrix3D& tm, const Vector3D& v);

    /** Получть результат умножения матрицы на вектор.
        @param A матрица.
        @param B вектор (столбец).
        @return матрица r = A * B.*/
    friend const Matrix3D operator*(const Matrix3D& A, const Matrix3D& B);

};

/** Трехмерный вектор.*/
struct Vector3D
{
    /** Индексы трехмерного вектора.*/
    enum CARTESIAN_IDX
    {
        /** Проекция на ось X*/
        X,

        /** Проекция на ось Y*/
        Y,

        /** Проекция на ось Z*/
        Z
    };

    /** Компоненты трехмерного вектора.*/
    double value[3];

    /** Конструктор по-умолчанию.*/
    Vector3D();

    /** Конструктор с параметрами
        @param x проекция вектора на ось x.
        @param y проекция вектора на ось y.
        @param z проекция вектора на ось z. */
    Vector3D(double x,double y,double z);

    /** Инициализировать вектор.
        @param x проекция вектора на ось x.
        @param y проекция вектора на ось y.
        @param z проекция вектора на ось z. */
    void fromValue(double x,double y,double z);

    /** Инициализировать вектор из сферических координат.
        @param r радиус.
        @param theta зенит.
        @param phi азимут. */
    void fromPolar(double r, double theta, double phi);

    /** Конвертировать вектор в набор сферических координат.
        @param r радиус.
        @param theta зенит.
        @param phi азимут.*/
    void toPolar(double& r, double& theta, double& phi);

    /** Получить модуль вектора.*/
    double getModulus();

    /** Получить модуль проекции на плоскость XY.*/
    double getModulusXY();


    double x() {return value[X];};
    double y() {return value[Y];};
    double z() {return value[Z];};


    /** Получть результат умножения вектора на матрицу.
        @param v вектор (строка).
        @param tm матрица.
        @return вектор r = v*tm.*/
    friend const Vector3D operator*(const Vector3D& v, const Matrix3D& tm);

    /** Получть результат умножения матрицы на вектор.
        @param tm матрица.
        @param v вектор (столбец).
        @return вектор r = v * tm.*/
    friend const Vector3D operator*(const Matrix3D& tm, const  Vector3D& v);

    /** Получть результат сложения двух векторов.
        @param a вектор.
        @param b вектор.
        @return вектор r = a + b.*/
    friend const Vector3D operator +(const Vector3D &a, const Vector3D &b);

    /** Получить результат поэлементного сложения вектора и скаляра.
        @param a вектор.
        @param c скаляр.
        @return вектор r[i] = a[i] + c.*/
    friend const Vector3D operator +(const Vector3D &a, double c);

    /** Получить результат деления вектора на скаляр.
        @param a вектор.
        @param c скаляр.
        @return вектор r = a / c.*/
    friend const Vector3D operator /(const Vector3D &a, double c);

    /** Получить результат умножения вектора на скаляр.
        @param a вектор.
        @param c скаляр.
        @return вектор r = a * c.*/
    friend const Vector3D operator *(const Vector3D &a, double c);

    /** Получить результат умножения скаляра на вектор.
        @param a вектор.
        @param c скаляр.
        @return вектор r = a * c.*/
    friend const Vector3D operator *(double c, const Vector3D &a);

    /** Получить результат вычитания векторов.
        @param a вектор.
        @param c вектор.
        @return вектор r = a - b.*/
    friend const Vector3D operator -(const Vector3D &a, const Vector3D &b);

    /** Получить результат скалярного произведения векторов.
        @param a вектор.
        @param c вектор.
        @return скаляр r = (a, b).*/
    friend double operator *(const Vector3D &a, const Vector3D &b);

    /** Получить результат векторного произведения векторов.
        @param a вектор.
        @param c вектор.
        @return вектор r = [a, b].*/
    friend const Vector3D operator ^(const Vector3D &a, const Vector3D &b);

    /** Инвертировать вектор.
        @param a вектор.
        @return вектор r = -a.*/
    friend const Vector3D operator -(const Vector3D &a);

    /** Получить результат умножения матрицы 3 х n на вектор.
        @param a вектор.
        @param c вектор.
        @return вектор r = [a, b].*/
    friend const Vector3D operator *(const Vector3D* vaT, const Vector3D &a);
};

#endif //BNO_VECTORMATRIX_H
