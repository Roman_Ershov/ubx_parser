//
// Created by yelowt on 24.10.17.
//

#include "VectorMatrix3D.h"
#include <cmath>


Vector3D::Vector3D() = default;

Vector3D::Vector3D(double x,double y,double z)
{
    value[X] = x;   value[Y] = y;   value[Z] = z;
}

void Vector3D::fromValue(double x,double y,double z)
{
    value[X] = x;   value[Y] = y;   value[Z] = z;
}

void Vector3D::fromPolar(double r, double theta, double phi)
{
    value[X] = r * cos(theta) * cos(phi);
    value[Y] = r * cos(theta) * sin(phi);
    value[Z] = r * sin(theta);
}

void Vector3D::toPolar(double& r, double& theta, double& phi)
{
    r = getModulus();
    theta = asin(value[Z]/r);
    phi = atan2(value[Y], value[X]);
}

double Vector3D::getModulus()
{
    return sqrt(pow(value[X],2) + pow(value[Y],2) + pow(value[Z],2));
}

double Vector3D::getModulusXY()
{
    return sqrt(pow(value[X],2) + pow(value[Y],2));
}

const Vector3D operator *(const Vector3D& v, const Matrix3D& tm)
{
    Vector3D result;
    for (int i = 0 ; i < 3; i++)
    {
        double s = 0;
        for (int k = 0 ; k < 3; k++)
        {
            s+= v.value[k] * tm.value[3 * k + i];
        }
        result.value[i] = s;
    }
    return result;
}

const Vector3D operator *(const Matrix3D& tm, const Vector3D& v)
{
    Vector3D result;
    for (int i = 0 ; i < 3; i++)
    {
        double s = 0;
        for (int k = 0 ; k < 3; k++)
        {
            s+= v.value[k] * tm.value[3 * i + k];
        }
        result.value[i] = s;
    }
    return result;
}

const Vector3D operator +(const Vector3D &a, const Vector3D &b)
{
    return {a.value[Vector3D::X] + b.value[Vector3D::X],
            a.value[Vector3D::Y] + b.value[Vector3D::Y],
            a.value[Vector3D::Z] + b.value[Vector3D::Z]};
}

const Vector3D operator -(const Vector3D &a, const Vector3D &b)
{
    return {a.value[Vector3D::X] - b.value[Vector3D::X],
            a.value[Vector3D::Y] - b.value[Vector3D::Y],
            a.value[Vector3D::Z] - b.value[Vector3D::Z]};
}

const Vector3D operator +(const Vector3D &a, double c)
{
    return {a.value[Vector3D::X] + c,
            a.value[Vector3D::Y] + c,
            a.value[Vector3D::Z] + c};
}

const Vector3D operator /(const Vector3D &a, double c)
{
    return {a.value[Vector3D::X]/c,
            a.value[Vector3D::Y]/c,
            a.value[Vector3D::Z]/c};
}

const Vector3D operator *(const Vector3D &a, double c)
{
    return {a.value[Vector3D::X]*c, a.value[Vector3D::Y]*c, a.value[Vector3D::Z]*c};
}

const Vector3D operator *(double c, const Vector3D &a)
{
    return {a.value[Vector3D::X]*c, a.value[Vector3D::Y]*c, a.value[Vector3D::Z]*c};
}

double operator *(const Vector3D &a, const Vector3D &b)
{
    return a.value[Vector3D::X]*b.value[Vector3D::X] +
           a.value[Vector3D::Y]*b.value[Vector3D::Y] +
           a.value[Vector3D::Z]*b.value[Vector3D::Z];
}

const Vector3D operator ^(const Vector3D &a, const Vector3D &b)
{
    return {
            a.value[Vector3D::Y] * b.value[Vector3D::Z] - a.value[Vector3D::Z] * b.value[Vector3D::Y],
            a.value[Vector3D::Z] * b.value[Vector3D::X] - a.value[Vector3D::X] * b.value[Vector3D::Z],
            a.value[Vector3D::X] * b.value[Vector3D::Y] - a.value[Vector3D::Y] * b.value[Vector3D::X]
    };
}

const Vector3D operator -(const Vector3D &a)
{
    return {-a.value[Vector3D::X], -a.value[Vector3D::Y], -a.value[Vector3D::Z]};
}

const Vector3D operator *(const Vector3D* vaT, const Vector3D &a)
{
    return {vaT[0]* a, vaT[1]* a, vaT[2]* a};
}

Matrix3D::Matrix3D() = default;
void Matrix3D::ToTranspose()
{
    double temp;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i > j)
            {
                temp = value[3 * i + j];
                value[3 * i + j] = value[i + 3 * j];
                value[i + 3 * j] = temp;
            }
        }
    }
}

Matrix3D Matrix3D:: operator-()
{
    Matrix3D res = *this;
    res.ToTranspose();
    return res;
}

const Matrix3D operator*(const Matrix3D& A, const Matrix3D& B)
{
    Matrix3D result;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            double s = 0;
            for (int k = 0; k < 3; k++)
            {
                s+= A.value[3 * i + k] * B.value[3 * k + j];
            }
            result.value[3 * i + j] = s;
        }
    }

    return result;
}


Matrix3D::Matrix3D(TURN_TYPE type, double angle)
{
    CreateTurnMatrix(type, angle);
}

Matrix3D::Matrix3D(TURN_TYPE type, double s, double c)
{
    CreateTurnMatrix(type, s, c);
}

void Matrix3D::CreateTurnMatrix(TURN_TYPE type, double angle)
{
    double c = cos(angle);
    double s = sin(angle);
    CreateTurnMatrix(type, s, c);
}

void Matrix3D::CreateTurnMatrix(TURN_TYPE type, double s, double c)
{
    switch(type)
    {
        case TURN_X:
        {
            value[0 + 0] = 1;    value[0 + 1] = 0;    value[0 + 2] = 0;
            value[3 + 0] = 0;    value[3 + 1] = c;    value[3 + 2] = s;
            value[6 + 0] = 0;    value[6 + 1] = -s;   value[6 + 2] = c;
            break;
        }
        case TURN_Y:
        {
            value[0 + 0] = c;    value[0 + 1] = 0;    value[0 + 2] = -s;
            value[3 + 0] = 0;    value[3 + 1] = 1;    value[3 + 2] = 0;
            value[6 + 0] = s;    value[6 + 1] = 0;    value[6 + 2] = c;
            break;
        }
        case TURN_Z:
        {
            value[0 + 0] = c;    value[0 + 1] = s;    value[0 + 2] = 0;
            value[3 + 0] = -s;   value[3 + 1] = c;    value[3 + 2] = 0;
            value[6 + 0] = 0;    value[6 + 1] = 0;    value[6 + 2] = 1;
            break;
        }
        default:
        {
            break;
        }
    }
}

