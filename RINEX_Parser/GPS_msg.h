#pragma once
#include "RINEX_Message.h"
#include "Message_factory.h"

namespace rinex
{

	struct GPS_msg_data
	{
		std::uint8_t sv_id;
		std::uint16_t year;
		std::uint8_t month;
		std::uint8_t day;
		std::uint8_t hour;
		std::uint8_t minute;
		std::uint8_t second;

		double crs;
		double delta_n;
		double m_0;
		double cuc;
		double ecc;
		double cus;
		double sqrt_a;
		double toe;
		double cic;
		double omega_0;
		double cis;
		double i_0;
		double crc;
		double omega;
		double omega_dot;
		double i_dot;
		double gps_week;

	};

	class GPS_msg : public RINEX_Message
	{
		std::shared_ptr<GPS_msg_data> parsed_data;

	public:
		GPS_msg(RINEX_Msg_Id msg_id,
					std::vector<std::uint8_t>&& data);

		void do_parse() override;
		std::shared_ptr<igsk_data> to_igsk() override;
	};

	class GPS_msg_factory : public Message_factory
	{
		std::shared_ptr<RINEX_Message> create(RINEX_Msg_Id msg_id,
						std::vector<std::uint8_t>&& data) override;
	};
}

