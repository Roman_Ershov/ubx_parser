﻿#pragma once

#using <mscorlib.dll>
#using <System.dll>
#using <System.Data.dll>
#using <System.Xml.dll>

using namespace System::Security::Permissions;
[assembly:SecurityPermissionAttribute(SecurityAction::RequestMinimum, SkipVerification=false)];
// 
// Этот исходный код был создан с помощью xsd, версия=4.6.1055.0.
// 
namespace RINEX_Parser {
    using namespace System;
    ref class Sat_Data;
    
    
    /// <summary>
///Represents a strongly typed in-memory cache of data.
///</summary>
    [System::Serializable, 
    System::ComponentModel::DesignerCategoryAttribute(L"code"), 
    System::ComponentModel::ToolboxItem(true), 
    System::Xml::Serialization::XmlSchemaProviderAttribute(L"GetTypedDataSetSchema"), 
    System::Xml::Serialization::XmlRootAttribute(L"Sat_Data"), 
    System::ComponentModel::Design::HelpKeywordAttribute(L"vs.data.DataSet")]
    public ref class Sat_Data : public ::System::Data::DataSet {
        public : ref class satDataTable;
        public : ref class satRow;
        public : ref class satRowChangeEvent;
        
        private: RINEX_Parser::Sat_Data::satDataTable^  tablesat;
        
        private: ::System::Data::SchemaSerializationMode _schemaSerializationMode;
        
        public : [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        delegate System::Void satRowChangeEventHandler(::System::Object^  sender, RINEX_Parser::Sat_Data::satRowChangeEvent^  e);
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        Sat_Data();
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        Sat_Data(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context);
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::Browsable(false), 
        System::ComponentModel::DesignerSerializationVisibility(::System::ComponentModel::DesignerSerializationVisibility::Content)]
        property RINEX_Parser::Sat_Data::satDataTable^  sat {
            RINEX_Parser::Sat_Data::satDataTable^  get();
        }
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::BrowsableAttribute(true), 
        System::ComponentModel::DesignerSerializationVisibilityAttribute(::System::ComponentModel::DesignerSerializationVisibility::Visible)]
        virtual property ::System::Data::SchemaSerializationMode SchemaSerializationMode {
            ::System::Data::SchemaSerializationMode get() override;
            System::Void set(::System::Data::SchemaSerializationMode value) override;
        }
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::DesignerSerializationVisibilityAttribute(::System::ComponentModel::DesignerSerializationVisibility::Hidden)]
        property ::System::Data::DataTableCollection^  Tables {
            ::System::Data::DataTableCollection^  get() new;
        }
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::DesignerSerializationVisibilityAttribute(::System::ComponentModel::DesignerSerializationVisibility::Hidden)]
        property ::System::Data::DataRelationCollection^  Relations {
            ::System::Data::DataRelationCollection^  get() new;
        }
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Void InitializeDerivedDataSet() override;
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Data::DataSet^  Clone() override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Boolean ShouldSerializeTables() override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Boolean ShouldSerializeRelations() override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Void ReadXmlSerializable(::System::Xml::XmlReader^  reader) override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Xml::Schema::XmlSchema^  GetSchemaSerializable() override;
        
        internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void InitVars();
        
        internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void InitVars(::System::Boolean initTable);
        
        private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void InitClass();
        
        private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Boolean ShouldSerializesat();
        
        private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void SchemaChanged(::System::Object^  sender, ::System::ComponentModel::CollectionChangeEventArgs^  e);
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        static ::System::Xml::Schema::XmlSchemaComplexType^  GetTypedDataSetSchema(::System::Xml::Schema::XmlSchemaSet^  xs);
        
        public : /// <summary>
///Represents the strongly named DataTable class.
///</summary>
        [System::Serializable, 
        System::Xml::Serialization::XmlSchemaProviderAttribute(L"GetTypedTableSchema")]
        ref class satDataTable : public ::System::Data::DataTable, public ::System::Collections::IEnumerable {
            
            private: ::System::Data::DataColumn^  columntime;
            
            private: ::System::Data::DataColumn^  columnsat_num;
            
            private: ::System::Data::DataColumn^  columnsat_type;
            
            private: ::System::Data::DataColumn^  columnx;
            
            private: ::System::Data::DataColumn^  columny;
            
            private: ::System::Data::DataColumn^  columnz;
            
            private: ::System::Data::DataColumn^  columnv_x;
            
            private: ::System::Data::DataColumn^  columnv_y;
            
            private: ::System::Data::DataColumn^  columnv_z;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event RINEX_Parser::Sat_Data::satRowChangeEventHandler^  satRowChanging;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event RINEX_Parser::Sat_Data::satRowChangeEventHandler^  satRowChanged;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event RINEX_Parser::Sat_Data::satRowChangeEventHandler^  satRowDeleting;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event RINEX_Parser::Sat_Data::satRowChangeEventHandler^  satRowDeleted;
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satDataTable();
            internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satDataTable(::System::Data::DataTable^  table);
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satDataTable(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context);
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  timeColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  sat_numColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  sat_typeColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  xColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  yColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  zColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  v_xColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  v_yColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  v_zColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
            System::ComponentModel::Browsable(false)]
            property ::System::Int32 Count {
                ::System::Int32 get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property RINEX_Parser::Sat_Data::satRow^  default [::System::Int32 ] {
                RINEX_Parser::Sat_Data::satRow^  get(::System::Int32 index);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void AddsatRow(RINEX_Parser::Sat_Data::satRow^  row);
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            RINEX_Parser::Sat_Data::satRow^  AddsatRow(
                        System::DateTime time, 
                        System::UInt32 sat_num, 
                        System::String^  sat_type, 
                        System::Double x, 
                        System::Double y, 
                        System::Double z, 
                        System::Double v_x, 
                        System::Double v_y, 
                        System::Double v_z);
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Collections::IEnumerator^  GetEnumerator();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Data::DataTable^  Clone() override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Data::DataTable^  CreateInstance() override;
            
            internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void InitVars();
            
            private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void InitClass();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            RINEX_Parser::Sat_Data::satRow^  NewsatRow();
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Data::DataRow^  NewRowFromBuilder(::System::Data::DataRowBuilder^  builder) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Type^  GetRowType() override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowChanged(::System::Data::DataRowChangeEventArgs^  e) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowChanging(::System::Data::DataRowChangeEventArgs^  e) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowDeleted(::System::Data::DataRowChangeEventArgs^  e) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowDeleting(::System::Data::DataRowChangeEventArgs^  e) override;
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void RemovesatRow(RINEX_Parser::Sat_Data::satRow^  row);
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            static ::System::Xml::Schema::XmlSchemaComplexType^  GetTypedTableSchema(::System::Xml::Schema::XmlSchemaSet^  xs);
        };
        
        public : /// <summary>
///Represents strongly named DataRow class.
///</summary>
        ref class satRow : public ::System::Data::DataRow {
            
            private: RINEX_Parser::Sat_Data::satDataTable^  tablesat;
            
            internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satRow(::System::Data::DataRowBuilder^  rb);
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::DateTime time {
                System::DateTime get();
                System::Void set(System::DateTime value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::UInt32 sat_num {
                System::UInt32 get();
                System::Void set(System::UInt32 value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::String^  sat_type {
                System::String^  get();
                System::Void set(System::String^  value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Double x {
                System::Double get();
                System::Void set(System::Double value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Double y {
                System::Double get();
                System::Void set(System::Double value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Double z {
                System::Double get();
                System::Void set(System::Double value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Double v_x {
                System::Double get();
                System::Void set(System::Double value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Double v_y {
                System::Double get();
                System::Void set(System::Double value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Double v_z {
                System::Double get();
                System::Void set(System::Double value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean IstimeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void SettimeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean Issat_numNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void Setsat_numNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean Issat_typeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void Setsat_typeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean IsxNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void SetxNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean IsyNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void SetyNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean IszNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void SetzNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean Isv_xNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void Setv_xNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean Isv_yNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void Setv_yNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean Isv_zNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void Setv_zNull();
        };
        
        public : /// <summary>
///Row event argument class
///</summary>
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ref class satRowChangeEvent : public ::System::EventArgs {
            
            private: RINEX_Parser::Sat_Data::satRow^  eventRow;
            
            private: ::System::Data::DataRowAction eventAction;
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satRowChangeEvent(RINEX_Parser::Sat_Data::satRow^  row, ::System::Data::DataRowAction action);
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property RINEX_Parser::Sat_Data::satRow^  Row {
                RINEX_Parser::Sat_Data::satRow^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataRowAction Action {
                ::System::Data::DataRowAction get();
            }
        };
    };
}
namespace RINEX_Parser {
    
    
    inline Sat_Data::Sat_Data() {
        this->BeginInit();
        this->InitClass();
        ::System::ComponentModel::CollectionChangeEventHandler^  schemaChangedHandler = gcnew ::System::ComponentModel::CollectionChangeEventHandler(this, &RINEX_Parser::Sat_Data::SchemaChanged);
        __super::Tables->CollectionChanged += schemaChangedHandler;
        __super::Relations->CollectionChanged += schemaChangedHandler;
        this->EndInit();
    }
    
    inline Sat_Data::Sat_Data(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context) : 
            ::System::Data::DataSet(info, context, false) {
        if (this->IsBinarySerialized(info, context) == true) {
            this->InitVars(false);
            ::System::ComponentModel::CollectionChangeEventHandler^  schemaChangedHandler1 = gcnew ::System::ComponentModel::CollectionChangeEventHandler(this, &RINEX_Parser::Sat_Data::SchemaChanged);
            this->Tables->CollectionChanged += schemaChangedHandler1;
            this->Relations->CollectionChanged += schemaChangedHandler1;
            return;
        }
        ::System::String^  strSchema = (cli::safe_cast<::System::String^  >(info->GetValue(L"XmlSchema", ::System::String::typeid)));
        if (this->DetermineSchemaSerializationMode(info, context) == ::System::Data::SchemaSerializationMode::IncludeSchema) {
            ::System::Data::DataSet^  ds = (gcnew ::System::Data::DataSet());
            ds->ReadXmlSchema((gcnew ::System::Xml::XmlTextReader((gcnew ::System::IO::StringReader(strSchema)))));
            if (ds->Tables[L"sat"] != nullptr) {
                __super::Tables->Add((gcnew RINEX_Parser::Sat_Data::satDataTable(ds->Tables[L"sat"])));
            }
            this->DataSetName = ds->DataSetName;
            this->Prefix = ds->Prefix;
            this->Namespace = ds->Namespace;
            this->Locale = ds->Locale;
            this->CaseSensitive = ds->CaseSensitive;
            this->EnforceConstraints = ds->EnforceConstraints;
            this->Merge(ds, false, ::System::Data::MissingSchemaAction::Add);
            this->InitVars();
        }
        else {
            this->ReadXmlSchema((gcnew ::System::Xml::XmlTextReader((gcnew ::System::IO::StringReader(strSchema)))));
        }
        this->GetSerializationData(info, context);
        ::System::ComponentModel::CollectionChangeEventHandler^  schemaChangedHandler = gcnew ::System::ComponentModel::CollectionChangeEventHandler(this, &RINEX_Parser::Sat_Data::SchemaChanged);
        __super::Tables->CollectionChanged += schemaChangedHandler;
        this->Relations->CollectionChanged += schemaChangedHandler;
    }
    
    inline RINEX_Parser::Sat_Data::satDataTable^  Sat_Data::sat::get() {
        return this->tablesat;
    }
    
    inline ::System::Data::SchemaSerializationMode Sat_Data::SchemaSerializationMode::get() {
        return this->_schemaSerializationMode;
    }
    inline System::Void Sat_Data::SchemaSerializationMode::set(::System::Data::SchemaSerializationMode value) {
        this->_schemaSerializationMode = __identifier(value);
    }
    
    inline ::System::Data::DataTableCollection^  Sat_Data::Tables::get() {
        return __super::Tables;
    }
    
    inline ::System::Data::DataRelationCollection^  Sat_Data::Relations::get() {
        return __super::Relations;
    }
    
    inline ::System::Void Sat_Data::InitializeDerivedDataSet() {
        this->BeginInit();
        this->InitClass();
        this->EndInit();
    }
    
    inline ::System::Data::DataSet^  Sat_Data::Clone() {
        RINEX_Parser::Sat_Data^  cln = (cli::safe_cast<RINEX_Parser::Sat_Data^  >(__super::Clone()));
        cln->InitVars();
        cln->SchemaSerializationMode = this->SchemaSerializationMode;
        return cln;
    }
    
    inline ::System::Boolean Sat_Data::ShouldSerializeTables() {
        return false;
    }
    
    inline ::System::Boolean Sat_Data::ShouldSerializeRelations() {
        return false;
    }
    
    inline ::System::Void Sat_Data::ReadXmlSerializable(::System::Xml::XmlReader^  reader) {
        if (this->DetermineSchemaSerializationMode(reader) == ::System::Data::SchemaSerializationMode::IncludeSchema) {
            this->Reset();
            ::System::Data::DataSet^  ds = (gcnew ::System::Data::DataSet());
            ds->ReadXml(reader);
            if (ds->Tables[L"sat"] != nullptr) {
                __super::Tables->Add((gcnew RINEX_Parser::Sat_Data::satDataTable(ds->Tables[L"sat"])));
            }
            this->DataSetName = ds->DataSetName;
            this->Prefix = ds->Prefix;
            this->Namespace = ds->Namespace;
            this->Locale = ds->Locale;
            this->CaseSensitive = ds->CaseSensitive;
            this->EnforceConstraints = ds->EnforceConstraints;
            this->Merge(ds, false, ::System::Data::MissingSchemaAction::Add);
            this->InitVars();
        }
        else {
            this->ReadXml(reader);
            this->InitVars();
        }
    }
    
    inline ::System::Xml::Schema::XmlSchema^  Sat_Data::GetSchemaSerializable() {
        ::System::IO::MemoryStream^  stream = (gcnew ::System::IO::MemoryStream());
        this->WriteXmlSchema((gcnew ::System::Xml::XmlTextWriter(stream, nullptr)));
        stream->Position = 0;
        return ::System::Xml::Schema::XmlSchema::Read((gcnew ::System::Xml::XmlTextReader(stream)), nullptr);
    }
    
    inline ::System::Void Sat_Data::InitVars() {
        this->InitVars(true);
    }
    
    inline ::System::Void Sat_Data::InitVars(::System::Boolean initTable) {
        this->tablesat = (cli::safe_cast<RINEX_Parser::Sat_Data::satDataTable^  >(__super::Tables[L"sat"]));
        if (initTable == true) {
            if (this->tablesat != nullptr) {
                this->tablesat->InitVars();
            }
        }
    }
    
    inline ::System::Void Sat_Data::InitClass() {
        this->DataSetName = L"Sat_Data";
        this->Prefix = L"";
        this->EnforceConstraints = true;
        this->SchemaSerializationMode = ::System::Data::SchemaSerializationMode::IncludeSchema;
        this->tablesat = (gcnew RINEX_Parser::Sat_Data::satDataTable());
        __super::Tables->Add(this->tablesat);
    }
    
    inline ::System::Boolean Sat_Data::ShouldSerializesat() {
        return false;
    }
    
    inline ::System::Void Sat_Data::SchemaChanged(::System::Object^  sender, ::System::ComponentModel::CollectionChangeEventArgs^  e) {
        if (e->Action == ::System::ComponentModel::CollectionChangeAction::Remove) {
            this->InitVars();
        }
    }
    
    inline ::System::Xml::Schema::XmlSchemaComplexType^  Sat_Data::GetTypedDataSetSchema(::System::Xml::Schema::XmlSchemaSet^  xs) {
        RINEX_Parser::Sat_Data^  ds = (gcnew RINEX_Parser::Sat_Data());
        ::System::Xml::Schema::XmlSchemaComplexType^  type = (gcnew ::System::Xml::Schema::XmlSchemaComplexType());
        ::System::Xml::Schema::XmlSchemaSequence^  sequence = (gcnew ::System::Xml::Schema::XmlSchemaSequence());
        ::System::Xml::Schema::XmlSchemaAny^  any = (gcnew ::System::Xml::Schema::XmlSchemaAny());
        any->Namespace = ds->Namespace;
        sequence->Items->Add(any);
        type->Particle = sequence;
        ::System::Xml::Schema::XmlSchema^  dsSchema = ds->GetSchemaSerializable();
        if (xs->Contains(dsSchema->TargetNamespace)) {
            ::System::IO::MemoryStream^  s1 = (gcnew ::System::IO::MemoryStream());
            ::System::IO::MemoryStream^  s2 = (gcnew ::System::IO::MemoryStream());
            try {
                ::System::Xml::Schema::XmlSchema^  schema = nullptr;
                dsSchema->Write(s1);
                for (                ::System::Collections::IEnumerator^  schemas = xs->Schemas(dsSchema->TargetNamespace)->GetEnumerator(); schemas->MoveNext();                 ) {
                    schema = (cli::safe_cast<::System::Xml::Schema::XmlSchema^  >(schemas->Current));
                    s2->SetLength(0);
                    schema->Write(s2);
                    if (s1->Length == s2->Length) {
                        s1->Position = 0;
                        s2->Position = 0;
                        for (                        ; ((s1->Position != s1->Length) 
                                    && (s1->ReadByte() == s2->ReadByte()));                         ) {
                            ;
                        }
                        if (s1->Position == s1->Length) {
                            return type;
                        }
                    }
                }
            }
            finally {
                if (s1 != nullptr) {
                    s1->Close();
                }
                if (s2 != nullptr) {
                    s2->Close();
                }
            }
        }
        xs->Add(dsSchema);
        return type;
    }
    
    
    inline Sat_Data::satDataTable::satDataTable() {
        this->TableName = L"sat";
        this->BeginInit();
        this->InitClass();
        this->EndInit();
    }
    
    inline Sat_Data::satDataTable::satDataTable(::System::Data::DataTable^  table) {
        this->TableName = table->TableName;
        if (table->CaseSensitive != table->DataSet->CaseSensitive) {
            this->CaseSensitive = table->CaseSensitive;
        }
        if (table->Locale->ToString() != table->DataSet->Locale->ToString()) {
            this->Locale = table->Locale;
        }
        if (table->Namespace != table->DataSet->Namespace) {
            this->Namespace = table->Namespace;
        }
        this->Prefix = table->Prefix;
        this->MinimumCapacity = table->MinimumCapacity;
    }
    
    inline Sat_Data::satDataTable::satDataTable(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context) : 
            ::System::Data::DataTable(info, context) {
        this->InitVars();
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::timeColumn::get() {
        return this->columntime;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::sat_numColumn::get() {
        return this->columnsat_num;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::sat_typeColumn::get() {
        return this->columnsat_type;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::xColumn::get() {
        return this->columnx;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::yColumn::get() {
        return this->columny;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::zColumn::get() {
        return this->columnz;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::v_xColumn::get() {
        return this->columnv_x;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::v_yColumn::get() {
        return this->columnv_y;
    }
    
    inline ::System::Data::DataColumn^  Sat_Data::satDataTable::v_zColumn::get() {
        return this->columnv_z;
    }
    
    inline ::System::Int32 Sat_Data::satDataTable::Count::get() {
        return this->Rows->Count;
    }
    
    inline RINEX_Parser::Sat_Data::satRow^  Sat_Data::satDataTable::default::get(::System::Int32 index) {
        return (cli::safe_cast<RINEX_Parser::Sat_Data::satRow^  >(this->Rows[index]));
    }
    
    inline ::System::Void Sat_Data::satDataTable::AddsatRow(RINEX_Parser::Sat_Data::satRow^  row) {
        this->Rows->Add(row);
    }
    
    inline RINEX_Parser::Sat_Data::satRow^  Sat_Data::satDataTable::AddsatRow(
                System::DateTime time, 
                System::UInt32 sat_num, 
                System::String^  sat_type, 
                System::Double x, 
                System::Double y, 
                System::Double z, 
                System::Double v_x, 
                System::Double v_y, 
                System::Double v_z) {
        RINEX_Parser::Sat_Data::satRow^  rowsatRow = (cli::safe_cast<RINEX_Parser::Sat_Data::satRow^  >(this->NewRow()));
        cli::array< ::System::Object^  >^  columnValuesArray = gcnew cli::array< ::System::Object^  >(9) {time, sat_num, sat_type, 
            x, y, z, v_x, v_y, v_z};
        rowsatRow->ItemArray = columnValuesArray;
        this->Rows->Add(rowsatRow);
        return rowsatRow;
    }
    
    inline ::System::Collections::IEnumerator^  Sat_Data::satDataTable::GetEnumerator() {
        return this->Rows->GetEnumerator();
    }
    
    inline ::System::Data::DataTable^  Sat_Data::satDataTable::Clone() {
        RINEX_Parser::Sat_Data::satDataTable^  cln = (cli::safe_cast<RINEX_Parser::Sat_Data::satDataTable^  >(__super::Clone()));
        cln->InitVars();
        return cln;
    }
    
    inline ::System::Data::DataTable^  Sat_Data::satDataTable::CreateInstance() {
        return (gcnew RINEX_Parser::Sat_Data::satDataTable());
    }
    
    inline ::System::Void Sat_Data::satDataTable::InitVars() {
        this->columntime = __super::Columns[L"time"];
        this->columnsat_num = __super::Columns[L"sat_num"];
        this->columnsat_type = __super::Columns[L"sat_type"];
        this->columnx = __super::Columns[L"x"];
        this->columny = __super::Columns[L"y"];
        this->columnz = __super::Columns[L"z"];
        this->columnv_x = __super::Columns[L"v_x"];
        this->columnv_y = __super::Columns[L"v_y"];
        this->columnv_z = __super::Columns[L"v_z"];
    }
    
    inline ::System::Void Sat_Data::satDataTable::InitClass() {
        this->columntime = (gcnew ::System::Data::DataColumn(L"time", ::System::DateTime::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columntime);
        this->columnsat_num = (gcnew ::System::Data::DataColumn(L"sat_num", ::System::UInt32::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnsat_num);
        this->columnsat_type = (gcnew ::System::Data::DataColumn(L"sat_type", ::System::String::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnsat_type);
        this->columnx = (gcnew ::System::Data::DataColumn(L"x", ::System::Double::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnx);
        this->columny = (gcnew ::System::Data::DataColumn(L"y", ::System::Double::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columny);
        this->columnz = (gcnew ::System::Data::DataColumn(L"z", ::System::Double::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnz);
        this->columnv_x = (gcnew ::System::Data::DataColumn(L"v_x", ::System::Double::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnv_x);
        this->columnv_y = (gcnew ::System::Data::DataColumn(L"v_y", ::System::Double::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnv_y);
        this->columnv_z = (gcnew ::System::Data::DataColumn(L"v_z", ::System::Double::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnv_z);
        this->columntime->Namespace = L"";
        this->columnsat_num->Namespace = L"";
        this->columnsat_type->Namespace = L"";
        this->columnx->Namespace = L"";
        this->columny->Namespace = L"";
        this->columnz->Namespace = L"";
        this->columnv_x->Namespace = L"";
        this->columnv_y->Namespace = L"";
        this->columnv_z->Namespace = L"";
    }
    
    inline RINEX_Parser::Sat_Data::satRow^  Sat_Data::satDataTable::NewsatRow() {
        return (cli::safe_cast<RINEX_Parser::Sat_Data::satRow^  >(this->NewRow()));
    }
    
    inline ::System::Data::DataRow^  Sat_Data::satDataTable::NewRowFromBuilder(::System::Data::DataRowBuilder^  builder) {
        return (gcnew RINEX_Parser::Sat_Data::satRow(builder));
    }
    
    inline ::System::Type^  Sat_Data::satDataTable::GetRowType() {
        return RINEX_Parser::Sat_Data::satRow::typeid;
    }
    
    inline ::System::Void Sat_Data::satDataTable::OnRowChanged(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowChanged(e);
        {
            this->satRowChanged(this, (gcnew RINEX_Parser::Sat_Data::satRowChangeEvent((cli::safe_cast<RINEX_Parser::Sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Sat_Data::satDataTable::OnRowChanging(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowChanging(e);
        {
            this->satRowChanging(this, (gcnew RINEX_Parser::Sat_Data::satRowChangeEvent((cli::safe_cast<RINEX_Parser::Sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Sat_Data::satDataTable::OnRowDeleted(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowDeleted(e);
        {
            this->satRowDeleted(this, (gcnew RINEX_Parser::Sat_Data::satRowChangeEvent((cli::safe_cast<RINEX_Parser::Sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Sat_Data::satDataTable::OnRowDeleting(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowDeleting(e);
        {
            this->satRowDeleting(this, (gcnew RINEX_Parser::Sat_Data::satRowChangeEvent((cli::safe_cast<RINEX_Parser::Sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Sat_Data::satDataTable::RemovesatRow(RINEX_Parser::Sat_Data::satRow^  row) {
        this->Rows->Remove(row);
    }
    
    inline ::System::Xml::Schema::XmlSchemaComplexType^  Sat_Data::satDataTable::GetTypedTableSchema(::System::Xml::Schema::XmlSchemaSet^  xs) {
        ::System::Xml::Schema::XmlSchemaComplexType^  type = (gcnew ::System::Xml::Schema::XmlSchemaComplexType());
        ::System::Xml::Schema::XmlSchemaSequence^  sequence = (gcnew ::System::Xml::Schema::XmlSchemaSequence());
        RINEX_Parser::Sat_Data^  ds = (gcnew RINEX_Parser::Sat_Data());
        ::System::Xml::Schema::XmlSchemaAny^  any1 = (gcnew ::System::Xml::Schema::XmlSchemaAny());
        any1->Namespace = L"http://www.w3.org/2001/XMLSchema";
        any1->MinOccurs = ::System::Decimal(0);
        any1->MaxOccurs = ::System::Decimal::MaxValue;
        any1->ProcessContents = ::System::Xml::Schema::XmlSchemaContentProcessing::Lax;
        sequence->Items->Add(any1);
        ::System::Xml::Schema::XmlSchemaAny^  any2 = (gcnew ::System::Xml::Schema::XmlSchemaAny());
        any2->Namespace = L"urn:schemas-microsoft-com:xml-diffgram-v1";
        any2->MinOccurs = ::System::Decimal(1);
        any2->ProcessContents = ::System::Xml::Schema::XmlSchemaContentProcessing::Lax;
        sequence->Items->Add(any2);
        ::System::Xml::Schema::XmlSchemaAttribute^  attribute1 = (gcnew ::System::Xml::Schema::XmlSchemaAttribute());
        attribute1->Name = L"namespace";
        attribute1->FixedValue = ds->Namespace;
        type->Attributes->Add(attribute1);
        ::System::Xml::Schema::XmlSchemaAttribute^  attribute2 = (gcnew ::System::Xml::Schema::XmlSchemaAttribute());
        attribute2->Name = L"tableTypeName";
        attribute2->FixedValue = L"satDataTable";
        type->Attributes->Add(attribute2);
        type->Particle = sequence;
        ::System::Xml::Schema::XmlSchema^  dsSchema = ds->GetSchemaSerializable();
        if (xs->Contains(dsSchema->TargetNamespace)) {
            ::System::IO::MemoryStream^  s1 = (gcnew ::System::IO::MemoryStream());
            ::System::IO::MemoryStream^  s2 = (gcnew ::System::IO::MemoryStream());
            try {
                ::System::Xml::Schema::XmlSchema^  schema = nullptr;
                dsSchema->Write(s1);
                for (                ::System::Collections::IEnumerator^  schemas = xs->Schemas(dsSchema->TargetNamespace)->GetEnumerator(); schemas->MoveNext();                 ) {
                    schema = (cli::safe_cast<::System::Xml::Schema::XmlSchema^  >(schemas->Current));
                    s2->SetLength(0);
                    schema->Write(s2);
                    if (s1->Length == s2->Length) {
                        s1->Position = 0;
                        s2->Position = 0;
                        for (                        ; ((s1->Position != s1->Length) 
                                    && (s1->ReadByte() == s2->ReadByte()));                         ) {
                            ;
                        }
                        if (s1->Position == s1->Length) {
                            return type;
                        }
                    }
                }
            }
            finally {
                if (s1 != nullptr) {
                    s1->Close();
                }
                if (s2 != nullptr) {
                    s2->Close();
                }
            }
        }
        xs->Add(dsSchema);
        return type;
    }
    
    
    inline Sat_Data::satRow::satRow(::System::Data::DataRowBuilder^  rb) : 
            ::System::Data::DataRow(rb) {
        this->tablesat = (cli::safe_cast<RINEX_Parser::Sat_Data::satDataTable^  >(this->Table));
    }
    
    inline System::DateTime Sat_Data::satRow::time::get() {
        try {
            return (cli::safe_cast<::System::DateTime >(this[this->tablesat->timeColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'time\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Sat_Data::satRow::time::set(System::DateTime value) {
        this[this->tablesat->timeColumn] = value;
    }
    
    inline System::UInt32 Sat_Data::satRow::sat_num::get() {
        try {
            return (cli::safe_cast<::System::UInt32 >(this[this->tablesat->sat_numColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'sat_num\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Sat_Data::satRow::sat_num::set(System::UInt32 value) {
        this[this->tablesat->sat_numColumn] = value;
    }
    
    inline System::String^  Sat_Data::satRow::sat_type::get() {
        try {
            return (cli::safe_cast<::System::String^  >(this[this->tablesat->sat_typeColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'sat_type\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Sat_Data::satRow::sat_type::set(System::String^  value) {
        this[this->tablesat->sat_typeColumn] = value;
    }
    
    inline System::Double Sat_Data::satRow::x::get() {
        try {
            return (cli::safe_cast<::System::Double >(this[this->tablesat->xColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'x\' в таблице \'sat\' равно DBNull.", e));
        }
    }
    inline System::Void Sat_Data::satRow::x::set(System::Double value) {
        this[this->tablesat->xColumn] = value;
    }
    
    inline System::Double Sat_Data::satRow::y::get() {
        try {
            return (cli::safe_cast<::System::Double >(this[this->tablesat->yColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'y\' в таблице \'sat\' равно DBNull.", e));
        }
    }
    inline System::Void Sat_Data::satRow::y::set(System::Double value) {
        this[this->tablesat->yColumn] = value;
    }
    
    inline System::Double Sat_Data::satRow::z::get() {
        try {
            return (cli::safe_cast<::System::Double >(this[this->tablesat->zColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'z\' в таблице \'sat\' равно DBNull.", e));
        }
    }
    inline System::Void Sat_Data::satRow::z::set(System::Double value) {
        this[this->tablesat->zColumn] = value;
    }
    
    inline System::Double Sat_Data::satRow::v_x::get() {
        try {
            return (cli::safe_cast<::System::Double >(this[this->tablesat->v_xColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'v_x\' в таблице \'sat\' равно DBNull.", e));
        }
    }
    inline System::Void Sat_Data::satRow::v_x::set(System::Double value) {
        this[this->tablesat->v_xColumn] = value;
    }
    
    inline System::Double Sat_Data::satRow::v_y::get() {
        try {
            return (cli::safe_cast<::System::Double >(this[this->tablesat->v_yColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'v_y\' в таблице \'sat\' равно DBNull.", e));
        }
    }
    inline System::Void Sat_Data::satRow::v_y::set(System::Double value) {
        this[this->tablesat->v_yColumn] = value;
    }
    
    inline System::Double Sat_Data::satRow::v_z::get() {
        try {
            return (cli::safe_cast<::System::Double >(this[this->tablesat->v_zColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'v_z\' в таблице \'sat\' равно DBNull.", e));
        }
    }
    inline System::Void Sat_Data::satRow::v_z::set(System::Double value) {
        this[this->tablesat->v_zColumn] = value;
    }
    
    inline ::System::Boolean Sat_Data::satRow::IstimeNull() {
        return this->IsNull(this->tablesat->timeColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::SettimeNull() {
        this[this->tablesat->timeColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::Issat_numNull() {
        return this->IsNull(this->tablesat->sat_numColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::Setsat_numNull() {
        this[this->tablesat->sat_numColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::Issat_typeNull() {
        return this->IsNull(this->tablesat->sat_typeColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::Setsat_typeNull() {
        this[this->tablesat->sat_typeColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::IsxNull() {
        return this->IsNull(this->tablesat->xColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::SetxNull() {
        this[this->tablesat->xColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::IsyNull() {
        return this->IsNull(this->tablesat->yColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::SetyNull() {
        this[this->tablesat->yColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::IszNull() {
        return this->IsNull(this->tablesat->zColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::SetzNull() {
        this[this->tablesat->zColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::Isv_xNull() {
        return this->IsNull(this->tablesat->v_xColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::Setv_xNull() {
        this[this->tablesat->v_xColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::Isv_yNull() {
        return this->IsNull(this->tablesat->v_yColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::Setv_yNull() {
        this[this->tablesat->v_yColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Sat_Data::satRow::Isv_zNull() {
        return this->IsNull(this->tablesat->v_zColumn);
    }
    
    inline ::System::Void Sat_Data::satRow::Setv_zNull() {
        this[this->tablesat->v_zColumn] = ::System::Convert::DBNull;
    }
    
    
    inline Sat_Data::satRowChangeEvent::satRowChangeEvent(RINEX_Parser::Sat_Data::satRow^  row, ::System::Data::DataRowAction action) {
        this->eventRow = row;
        this->eventAction = action;
    }
    
    inline RINEX_Parser::Sat_Data::satRow^  Sat_Data::satRowChangeEvent::Row::get() {
        return this->eventRow;
    }
    
    inline ::System::Data::DataRowAction Sat_Data::satRowChangeEvent::Action::get() {
        return this->eventAction;
    }
}
