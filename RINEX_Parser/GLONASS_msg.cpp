#include "GLONASS_msg.h"
#include <string>
#include "VectorMatrix3D.h"
#include "TimeHandling_T.h"
#include <ctime>
#include "AngleArguments.h"
#include "GeoDot.h"

rinex::GLONASS_msg::GLONASS_msg(RINEX_Msg_Id msg_id, std::vector<std::uint8_t>&& data):
								RINEX_Message(msg_id, std::forward<std::vector<std::uint8_t>>(data))
{
	parsed_data = std::make_shared<GLONASS_msg_data>();
}

void rinex::GLONASS_msg::do_parse()
{
	auto iterator = data.begin() + 1;
	std::string cur_data;

	cur_data.push_back(*iterator++);
	cur_data.push_back(*iterator++);
	parsed_data->sv_id = std::stoi(cur_data);

	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->year = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->month = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->day = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->hour = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->minute = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->second = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != '\n') ++iterator;
	iterator+=5;
		
	parsed_data->x = parse_double(iterator);
	parsed_data->v_x = parse_double(iterator);
	cur_data.clear();

	while (*iterator != '\n') ++iterator;
	iterator += 5;

	parsed_data->y = parse_double(iterator);
	parsed_data->v_y = parse_double(iterator);

	while (*iterator != '\n') ++iterator;
	iterator += 5;

	parsed_data->z = parse_double(iterator);
	parsed_data->v_z = parse_double(iterator);
}

std::shared_ptr<rinex::igsk_data> rinex::GLONASS_msg::to_igsk()
{
	std::shared_ptr<igsk_data> igsk_data = std::make_shared<rinex::igsk_data>();
	igsk_data->gnss_type = std::string("GLONASS");
	igsk_data->sv_id = parsed_data->sv_id;
	igsk_data->utc_tm.tm_year = parsed_data->year - 1900;
	igsk_data->utc_tm.tm_mon = parsed_data->month - 1;
	igsk_data->utc_tm.tm_mday = parsed_data->day;
	igsk_data->utc_tm.tm_hour = parsed_data->hour;
	igsk_data->utc_tm.tm_min = parsed_data->minute;
	igsk_data->utc_tm.tm_sec = parsed_data->second;

	//Vector3D not_igsk = { parsed_data->x, parsed_data->y, parsed_data->z };
	//
	//std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> time_point = 
	//	std::chrono::system_clock::from_time_t(std::mktime(&igsk_data->utc_tm));

	//AstronomicTime at(time_point);
	//long double tt = at.GetTT();
	//long double tdb = at.GetTDB();
	//AngleArguments aa(tdb, tt);

	//Matrix3D transform_matrix = aa.rotation * aa.nutation * aa.precession;
	//Matrix3D back_transform_matrix = -transform_matrix;
	//Vector3D igsk = back_transform_matrix * not_igsk;

	//igsk_data->x = igsk.value[0] * 1e+3;
	//igsk_data->y = igsk.value[1] * 1e+3;
	//igsk_data->z = igsk.value[2] * 1e+3;

	//not_igsk = { parsed_data->v_x, parsed_data->v_y, parsed_data->v_z };
	////transform_matrix = aa.rotation * aa.nutation * aa.precession;
	////back_transform_matrix = -transform_matrix;
	//igsk = back_transform_matrix * not_igsk;

	//igsk_data->v_x = igsk.value[0] * 1e+3;
	//igsk_data->v_y = igsk.value[1] * 1e+3;
	//igsk_data->v_z = igsk.value[2] * 1e+3;

	igsk_data->x = parsed_data->x * 1e+3;
	igsk_data->y = parsed_data->y * 1e+3;
	igsk_data->z = parsed_data->z * 1e+3;
	igsk_data->v_x = parsed_data->v_x * 1e+3;
	igsk_data->v_y = parsed_data->v_y * 1e+3;
	igsk_data->v_z = parsed_data->v_z * 1e+3;

	return igsk_data;
}

std::shared_ptr<rinex::RINEX_Message> rinex::GLONASS_msg_factory::create(RINEX_Msg_Id msg_id, 
																		std::vector<std::uint8_t>&& data)
{
	return std::make_shared<GLONASS_msg>(msg_id, std::forward<std::vector<std::uint8_t>>(data));
}
