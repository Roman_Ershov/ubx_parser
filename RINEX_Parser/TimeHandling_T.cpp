#include "TimeHandling_T.h"
#include <cmath>


bool AstronomicTime:: auto_leap_seconds_mode = false;
double AstronomicTime:: leap_seconds = 37;

TGreenwichTime::TGreenwichTime()
{
    Create(2000,1,12,0,0,0);
}

TGreenwichTime::TGreenwichTime(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                               unsigned int hour_i,unsigned int minute_i,double second_i)
{
    Create(year_i,month_i,day_i,hour_i,minute_i,second_i);
}

void TGreenwichTime::Create(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                            unsigned int hour_i,unsigned int minute_i,double second_i)
{
    year=year_i;
    month=month_i;
    day=day_i;
    hour=hour_i;
    minute=minute_i;
    second=second_i;
}

TMoscowTime::TMoscowTime()
{
    Create(2000,1,15,0,0,0);
}

TMoscowTime::TMoscowTime(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                         unsigned int hour_i,unsigned int minute_i,double second_i)
{
    Create(year_i,month_i,day_i,hour_i,minute_i,second_i);
}

void TMoscowTime::Create(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                         unsigned int hour_i,unsigned int minute_i,double second_i)
{
    year=year_i;
    month=month_i;
    day=day_i;
    hour=hour_i;
    minute=minute_i;
    second=second_i;
}

TGreenwichTime TMoscowTime::ConvertToGreenwichTime()
{
    TGreenwichTime gt;
    gt.second=second;
    gt.minute=minute;
    bool shifth=false;
    int h=hour-3;
    if(h<0)
    {
        gt.hour = 24+h;
        shifth=true;
    }
    else
        gt.hour = h;
    if(shifth)
    {
        int d=day;
        d--;
        if(d<1)
        {
            if(month==5||month==7||month==8||month==10||month==12)
            {
                gt.day=30;
                gt.month=month-1;
                gt.year=year;
            }
            if(month==2||month==4||month==6||month==9||month==11)
            {
                gt.day=31;
                gt.month=month-1;
                gt.year=year;
            }
            if(month==3)
            {
                if(year%4==0)
                    gt.day=29;
                else
                    gt.day=28;
                gt.month=2;
                gt.year=year;
            }
            if(month==1)
            {
                gt.day=31;
                gt.month=12;
                gt.year=year-1;
            }
        }
        else
        {
            gt.day=d;
            gt.month=month;
            gt.year=year;
        }
    }
    else
    {
        gt.day=day;
        gt.month=month;
        gt.year=year;
    }
    return gt;
}

AstronomicTime::AstronomicTime()
{
    mjd=0;
}

AstronomicTime::AstronomicTime(time_point<system_clock, nanoseconds> tp)
{
    Create(tp);
}



AstronomicTime::AstronomicTime(TGreenwichTime gt)
{
    Create(gt);
}

AstronomicTime::AstronomicTime(TMoscowTime mt)
{
    Create(mt);
}

void AstronomicTime::Create(TGreenwichTime gt)
{
    int year = gt.year-1900;
    int month = gt.month-3;
    if(month<0)
    {
        month=month+12;
        year=year-1;
    }
    mjd = 15078.0 + 365.0*year + int(year/4.0) + int(0.5 + 30.6*month);
    mjd += gt.day + gt.hour/24.0 + gt.minute/1440.0 + gt.second/86400.0; //(для Гринвича).
}

void AstronomicTime::Create(time_point<system_clock, nanoseconds> tp)
{
    auto tp_secs = time_point_cast<seconds>(tp);
    time_t tt = system_clock::to_time_t(tp_secs);
  //  tm tp_tt = * localtime(&tt);
	tm tp_tt;
	localtime_s(&tp_tt, &tt);
    int year = tp_tt.tm_year;
    int month = tp_tt.tm_mon - 2;

    mjd = 15078.0 + 365.0*year+int(year/4.0) + int(0.5 + 30.6*month);
    mjd += tp_tt.tm_mday + tp_tt.tm_hour/24.0 + tp_tt.tm_min/1440.0 + tp_tt.tm_sec/86400.0;

    duration<double> diff = tp - tp_secs;
    double ddd = diff.count();
    mjd +=  ddd / 86400.0;
}

void AstronomicTime::Create(TMoscowTime mt)
{
    int year=mt.year-1900;
    int month=mt.month-3;
    if(month<0)
    {
        month=month+12;
        year=year-1;
    }
    mjd=15078.0+365.0*year+int(year/4.0) + int(0.5+30.6*month);
    mjd=mjd+mt.day+mt.hour/24.0+mt.minute/1440.0+mt.second/86400.0-0.125; //(для Москвы)
}

TGreenwichTime AstronomicTime::GetGreenwichTime()
{
    TGreenwichTime cd;
    double rd=int(mjd)-15078.0;//Гринвич
    int nd=int(rd);
    int nz=int(rd/1461.01);
    int na=nd-1461*nz;
    int nb=int(na/365.25);
    cd.year=4*nz+nb+1900;
    if(na==1461)
    {
        cd.month=2;
        cd.day=29;
    }
    else
    {
        nz=na-365*nb;
        int ma=int((nz-0.5)/30.6);
        cd.month=ma+3;
        cd.day=nz-int(30.6*cd.month-91.3);
        if(cd.month>12)
        {
            cd.month=cd.month-12;
            cd.year=cd.year+1;
        }
    }
    double sp=24.0*(mjd-int(mjd));//Гринвич
    cd.hour=int(sp);
    sp=60.0*(sp-cd.hour);
    cd.minute=int(sp);
    cd.second=60.0*(sp-cd.minute);
    return cd;
}

// http://sunearth.gsfc.nasa.gov/eclipse/SEcat5/deltatpoly.html.
double AstronomicTime::GetTT()
{
    TGreenwichTime cd=GetGreenwichTime();
    double y=cd.year+(cd.month-0.5)/12.0;
    double t,dt;

 //   if (auto_leap_seconds_mode)
 //   {
        if      (cd.year < 1941)  dt = 29.0;
        else if (cd.year >= 1941 && cd.year < 1961)
        {
            t = y-1950;
            dt = 29.07 + 0.407*t - t*t/233 + t*t*t / 2547;
        }
        else if(cd.year>=1961 && cd.year < 1986)
        {
            t = y - 1975;
            dt = 45.45 + 1.067*t - t*t/260 - t*t*t / 718;
        }
        else if(cd.year >= 1986 && cd.year < 2005)
        {
            t = y - 2000;
            dt = 63.86 + 0.3345 * t - 0.060374 * t*t + 0.0017275 * t*t*t + 0.000651814 * t*t*t*t + 0.00002373599 * t*t*t*t*t;
        }
        else if(cd.year >= 2005 && cd.year < 2050)
        {
            t = y - 2000;
            dt = 62.92 + 0.32217 * t + 0.005589 * t*t ;
        }
        else if(cd.year >= 2050 && cd.year < 2150)
        {
            dt = -20 + 32 * ((y-1820)/100)*((y-1820)/100) - 0.5628 * (2150 - y);
        }
        else //if(cd.year > 2150)
        {
            t = (y-1820)/100.0;
            dt = -20 + 32 * t*t;
        }
  //  }
  /*  else
    {
		dt = 32.184 + leap_seconds;
    }*/

    return mjd+dt/86400.0;
}

double AstronomicTime::GetTDB()
{
    double tt=GetTT();
    double d = (tt-51544.5)/36525.0;
    double g=0.017453*(357.258+35999.050*d);
    double tdb=tt+(0.001658*sin(g+0.0167*sin(g)))/86400.0;
    return tdb;
}

void AstronomicTime::AddToMJD(double dt)
{
    mjd+=dt/86400.0;
}

double AstronomicTime::GetMJD()
{
    return mjd;
}


