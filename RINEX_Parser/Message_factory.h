#pragma once
#include <memory>
#include "RINEX_Message.h"

namespace rinex
{
	class Message_factory
	{
	public:
		virtual std::shared_ptr<RINEX_Message> create(RINEX_Msg_Id msg_id,
														std::vector<std::uint8_t>&& data) = 0;
		virtual ~Message_factory() = default;
	};

}
