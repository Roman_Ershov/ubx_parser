//
// Created by yelowt on 24.10.17.
//

#include "AngleArguments.h"
#include <cmath>

AngleArguments::AngleArguments(long double t_dbc, long double UT1)
{
    rg = 0.017453292519943296;
    rs = 4.848136811095e-06;

    /** Время в юлианских столетиях от J2000.*/
    t_c = (t_dbc-51544.5)/36525.0;

    lambda = rg*(218.31643250+(481267.8812772222-(0.00161167-0.00000528*t_c)*t_c)*t_c);
    l = rg*(134.96298139+(477198.8673980556+(0.00869722+0.00001778*t_c)*t_c)*t_c);
    ls = rg*(357.52772333+(35999.05034-(0.00016028+0.00000333*t_c)*t_c)*t_c);
    F = rg*(93.27191028+(483202.0175380555-(0.00368250-0.00000306*t_c)*t_c)*t_c);
    D = rg*(297.85036306+(445267.11148-(0.00191417-0.00000528*t_c)*t_c)*t_c);
    Calculate(t_dbc, UT1);
}

void AngleArguments::Calculate(long double t_dbc, long double UT1)
{
    /** Nutation */
    eps = rs*(84381.448-(46.815+(0.0059-0.001813*t_c)*t_c)*t_c);
    double dpsi = rs*(
                    (-17.1996-0.01742*t_c)*sin(lambda-F)
                    +(0.2062+0.00002*t_c)*sin(2.0*lambda-2.0*F)
                    -(1.3187+0.00016*t_c)*sin(2.0*lambda-2.0*D)
                    -(0.0016-0.00001*t_c)*sin(2.0*lambda+2.0*ls-2.0*D)
                    +(0.0129+0.00001*t_c)*sin(lambda+F-2.0*D)
                    -(0.0517-0.00012*t_c)*sin(2.0*lambda+ls-2.0*D)
                    +(0.0217-0.00005*t_c)*sin(2.0*lambda-ls-2.0*D)
                    +(0.0017-0.00001*t_c)*sin(2.0*ls)
                    +(0.1426-0.00034*t_c)*sin(ls)
                    +0.0048*sin(2.0*l-2.0*D)
                    +0.0046*sin(lambda-2.0*l+F)
                    -0.0022*sin(2.0*F-2.0*D)
                    -0.0015*sin(lambda+ls-F)
                    -0.0012*sin(lambda-ls-F)
                    +0.0011*sin(2.0*l-2.0*F)
                    +(-0.2274-0.00002*t_c)*sin(2*lambda)
                    +(0.0712+0.00001*t_c)*sin(l)
                    -(0.0386+0.00004*t_c)*sin(lambda+F)
                    -0.0301*sin(2*lambda+l)
                    -0.0158*sin(l-2*D)
                    +0.0123*sin(2*lambda-l)
                    +0.0063*sin(2*D)
                    +(0.0063+0.00001*t_c)*sin(lambda+l-F)
                    -(0.0058+0.00001*t_c)*sin(lambda-l-F)
                    -0.0059*sin(2*lambda-l+2*D)
                    -0.0051*sin(lambda+l+F)
                    -0.0038*sin(2*lambda+2*D)
                    +0.0029*sin(2*l)
                    +0.0029*sin(2*lambda+l-2*D)
                    -0.0031*sin(2*lambda+2*l)
                    +0.0026*sin(2*F)
                    +0.0021*sin(lambda-l+F)
                    +0.0016*sin(lambda-l-F+2*D)
                    -0.0013*sin(lambda+l-F-2*D)
                    -0.0010*sin(lambda-l+F+2*D)
    );
    double deps = rs*(
            +(9.2025+0.00089*t_c)*cos(lambda-F)
            -(0.0895-0.00005*t_c)*cos(2*lambda-2*F)
            -0.0024*cos(lambda-2*l+F)
            +(0.5736-0.00031*t_c)*cos(2*lambda-2*D)
            +(0.0054-0.00001*t_c)*cos(ls)
            +(0.0224-0.00006*t_c)*cos(2*lambda+ls-2*D)
            -(0.0095-0.00003*t_c)*cos(2*lambda-ls-2*D)
            -0.0070*cos(lambda+F-2*D)
            +(0.0977-0.00005*t_c)*cos(2*lambda)
            +0.0200*cos(lambda+F)
            +(0.0129-0.00001*t_c)*cos(2*lambda+l)
            -0.0053*cos(2*lambda-l)
            -0.0033*cos(lambda+l-F)
            +0.0032*cos(lambda-l-F)
            +0.0026*cos(2*lambda-l+2*D)
            +0.0027*cos(lambda+l+F)
            +0.0016*cos(2*lambda+2*D)
            -0.0012*cos(2*lambda+l-2*D)
            +0.0013*cos(2*lambda+2*l)
            -0.0010*cos(lambda-l+F)
    );

    Matrix3D Rx_deps(TURN_X, -eps - deps);
    Matrix3D Rz_dpsi(TURN_Z, -dpsi);
    Matrix3D Rx_eps(TURN_X, eps);
    nutation = Rx_deps*Rz_dpsi*Rx_eps;


    /** Precession. */
    double dzeta=rs*(2306.2181+(0.30188+0.017998*t_c)*t_c)*t_c;
    double theta=rs*(2004.3109-(0.42665+0.041833*t_c)*t_c)*t_c;
    double zeta=rs*(2306.2181+(1.09468+0.018203*t_c)*t_c)*t_c;

    Matrix3D Rz_zeta(TURN_Z, -zeta);
    Matrix3D Ry_theta(TURN_Y, theta);
    Matrix3D Rz_dzeta(TURN_Z, -dzeta);
    precession = Rz_zeta*Ry_theta*Rz_dzeta;

    /** Rotation. */
    double Tu = (int(UT1)-51544.5)/36525.0;
    double r = 6.300388098984891+(3.707456e-10 - 3.707e-14*Tu)*Tu;

    /** Гринвическое среднее звездное время. */
    double GMST = 1.753368559233266 + (628.3319706888409 +(6.770714e-06 - 4.51E-10*Tu)*Tu)*Tu;
    GMST = GMST + r*(UT1-int(UT1));
    /** Гринвическое истинное звездное время. */
    double GAST = GMST + dpsi*cos(eps);

    rotation =  Matrix3D(TURN_Z, GAST);
}