#pragma once
#include <fstream>
#include <string>
#include <vector>
#include "RINEX_Message.h"

class RINEX_Parser
{
	std::basic_ifstream<uint8_t> rnx_file;

	std::vector<std::shared_ptr<rinex::RINEX_Message>> messages;

public:

	explicit RINEX_Parser(const std::string& file);

	void do_parse();

	void export_to_xml();
	
	virtual ~RINEX_Parser();
};

