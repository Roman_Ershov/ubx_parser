#pragma once

#include <unordered_map>
#include "RINEX_Msg_Id.h"
#include "Message_factory.h"
#include "GPS_msg.h"
#include "GLONASS_msg.h"


namespace rinex
{
	class Message_type_factory
	{

		using typemap = std::unordered_map<RINEX_Msg_Id, std::shared_ptr<Message_factory>>;
		typemap _factories;

	public:
		void register_all_factories()
		{
			_factories.insert({ RINEX_Msg_Id::GPS, std::make_shared<GPS_msg_factory>() });
			_factories.insert({ RINEX_Msg_Id::GLONASS, std::make_shared<GLONASS_msg_factory>() });
		}

		std::shared_ptr<RINEX_Message> create(RINEX_Msg_Id msg_id,
											std::vector<std::uint8_t>&& data)
		{
			try
			{
				return _factories.at(msg_id)->create(msg_id, std::forward<std::vector<std::uint8_t>>(data));
			}

			catch (std::exception &e)
			{
				return nullptr;
			}
		}
	};
}
