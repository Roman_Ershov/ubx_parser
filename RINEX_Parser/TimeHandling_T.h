//
// Created by yelowt on 08.12.17.
//

#ifndef MMBL2_TIMEHANDLING_T_H
#define MMBL2_TIMEHANDLING_T_H

#include <chrono>

using namespace std;
using namespace chrono;

struct TGreenwichTime
{
    unsigned int year;//���
    unsigned int month;//�����
    unsigned int day;//����
    unsigned int hour;//���
    unsigned int minute;//������
    double second;//�������
    TGreenwichTime();
    TGreenwichTime(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                   unsigned int hour_i,unsigned int minute_i,double second_i);
    void Create(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                unsigned int hour_i,unsigned int minute_i,double second_i);
};
//
struct TMoscowTime
{
    unsigned int year;//���
    unsigned int month;//�����
    unsigned int day;//����
    unsigned int hour;//���
    unsigned int minute;//������
    double second;//�������
    TMoscowTime();
    TMoscowTime(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                unsigned int hour_i,unsigned int minute_i,double second_i);
    void Create(unsigned int year_i,unsigned int month_i,unsigned int day_i,
                unsigned int hour_i,unsigned int minute_i,double second_i);
    TGreenwichTime ConvertToGreenwichTime();
};
//
class AstronomicTime
{
public:
    static double leap_seconds;
    static bool auto_leap_seconds_mode;
    AstronomicTime();
    explicit AstronomicTime(time_point<system_clock, nanoseconds> tp);
    explicit AstronomicTime(TGreenwichTime gt);
    explicit AstronomicTime(TMoscowTime mt);
    void Create(TGreenwichTime gt);
    void Create(TMoscowTime mt);
    void Create(time_point<system_clock, nanoseconds> tp);
    TGreenwichTime GetGreenwichTime();//������� �����
    double GetTT();//����������� ������ �����
    double GetTDB();//���������������� ������������ �����
    void AddToMJD(double dt);//���������� ���� �� ������� (� ��������) � �������� �������� mjd
    double GetMJD();//��������� �������� �������� mjd
    double mjd;//���� � ���������������� ��������� ����.
};



#endif //MMBL2_TIMEHANDLING_T_H
