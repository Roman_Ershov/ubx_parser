#define _USE_MATH_DEFINES

#include "GPS_msg.h"
#include <string>
#include <chrono>
#include <ctime>
#include <valarray>
#include <cmath>
#include "VectorMatrix3D.h"
#include "TimeHandling_T.h"
#include "AngleArguments.h"


rinex::GPS_msg::GPS_msg(RINEX_Msg_Id msg_id, std::vector<std::uint8_t>&& data):
						RINEX_Message(msg_id, std::forward<std::vector<std::uint8_t>>(data))
{
	parsed_data = std::make_shared<GPS_msg_data>();
}

void rinex::GPS_msg::do_parse()
{
	auto iterator = data.begin() + 1;
	std::string cur_data;

	cur_data.push_back(*iterator++);
	cur_data.push_back(*iterator++);
	parsed_data->sv_id = std::stoi(cur_data);

	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->year = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->month = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->day = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->hour = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->minute = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != ' ') cur_data.push_back(*iterator++);
	parsed_data->second = std::stoi(cur_data);
	cur_data.clear();
	++iterator;

	while (*iterator != '\n') ++iterator;
	iterator += 5;

	parse_double(iterator);  //IODE, not use
	parsed_data->crs = parse_double(iterator);
	parsed_data->delta_n = parse_double(iterator);
	parsed_data->m_0 = parse_double(iterator);

	while (*iterator != '\n') ++iterator;
	iterator += 5;

	parsed_data->cuc = parse_double(iterator);
	parsed_data->ecc = parse_double(iterator);
	parsed_data->cus = parse_double(iterator);
	parsed_data->sqrt_a = parse_double(iterator);

	while (*iterator != '\n') ++iterator;
	iterator += 5;

	parsed_data->toe = parse_double(iterator);
	parsed_data->cic = parse_double(iterator);
	parsed_data->omega_0 = parse_double(iterator);
	parsed_data->cis = parse_double(iterator);
	
	while (*iterator != '\n') ++iterator;
	iterator += 5;

	parsed_data->i_0 = parse_double(iterator);
	parsed_data->crc = parse_double(iterator);
	parsed_data->omega = parse_double(iterator);
	parsed_data->omega_dot = parse_double(iterator);

	while (*iterator != '\n') ++iterator;
	iterator += 5;

	parsed_data->i_dot = parse_double(iterator);
	parse_double(iterator);
	parsed_data->gps_week = parse_double(iterator);
}

void descart_from_keplerian_elements(vector<double> keplerian_elements,
	time_point<system_clock, nanoseconds> &curr_time,
	time_point<system_clock, nanoseconds> &target_time,
	Vector3D &r,
	Vector3D &v)
{
	double gm = 398600440500000.0;
	double semi_major_axis = keplerian_elements[0];
	double eccentricity = keplerian_elements[1];
	double inclination = keplerian_elements[2];
	double ascending_node_ascension = keplerian_elements[3];
	double perigee_argument = keplerian_elements[4];
	double mean_anomaly_t0 = keplerian_elements[5];

	double mean_motion = sqrt(gm / (semi_major_axis * semi_major_axis * semi_major_axis));
	double mean_anomaly_t = mean_anomaly_t0 + mean_motion * ((target_time - curr_time).count() / 1000000000.0);

	double eps = 1e-8;  double e1 = mean_anomaly_t; double e0 = mean_anomaly_t;
	do
	{
		e0 = e1;
		e1 = e0 - (e0 - eccentricity * sin(e0) - mean_anomaly_t) / (1.0 - eccentricity * cos(e0));
	} while (abs(e1 - e0) > eps);
	double eccentricity_anomaly_t = e1;
	double sq1 = sqrt(1.0 + eccentricity);
	double sq2 = sqrt(1.0 - eccentricity);
	double sine = sin(eccentricity_anomaly_t / 2.0);
	double cose = cos(eccentricity_anomaly_t / 2.0);
	double true_anomaly_t = 2.0*atan2(sq1*sine, sq2*cose);

	double radius = semi_major_axis * (1.0 - eccentricity * cos(eccentricity_anomaly_t));

	Vector3D o_t(cos(true_anomaly_t), sin(true_anomaly_t), 0);
	o_t = o_t * radius;
	Vector3D os_t(-sin(eccentricity_anomaly_t), sqrt(1.0 - eccentricity * eccentricity)*cos(eccentricity_anomaly_t), 0);
	os_t = os_t * (sqrt(semi_major_axis*gm) / radius);

	Matrix3D turn_z_asc(TURN_Z, -ascending_node_ascension);
	Matrix3D turn_x_inc(TURN_X, -inclination);
	Matrix3D turn_z_per(TURN_Z, -perigee_argument);
	Matrix3D transform = turn_z_asc * turn_x_inc*turn_z_per;

	Vector3D r_t = transform * o_t;
	Vector3D v_t = transform * os_t;

	r = { r_t.x(),r_t.y(),r_t.z() };
	v = { v_t.x(), v_t.y(), v_t.z() };
};


std::shared_ptr<rinex::igsk_data> rinex::GPS_msg::to_igsk()
{
	std::shared_ptr<igsk_data> igsk_data = std::make_shared<rinex::igsk_data>();
	igsk_data->gnss_type = std::string("GPS");
	igsk_data->sv_id = parsed_data->sv_id;
	igsk_data->utc_tm.tm_year = parsed_data->year - 1900;
	igsk_data->utc_tm.tm_mon = parsed_data->month - 1;
	igsk_data->utc_tm.tm_mday = parsed_data->day;
	igsk_data->utc_tm.tm_hour = parsed_data->hour;
	igsk_data->utc_tm.tm_min = parsed_data->minute;
	igsk_data->utc_tm.tm_sec = parsed_data->second-18;
	
	time_point<system_clock, nanoseconds> epoch =
		std::chrono::system_clock::from_time_t(std::mktime(&igsk_data->utc_tm));
	

	constexpr double gm = 3.986005e+14;
	constexpr double we = 7.292115e-5;
	constexpr double eps = 1e-8;
	double mean_motion = sqrt(gm) / parsed_data->sqrt_a / parsed_data->sqrt_a / parsed_data->sqrt_a;
	double corrected_mean_motion = mean_motion + parsed_data->delta_n;

	std::tm tm = { 0 };
	tm.tm_mday = 6;
	tm.tm_mon = 0;
	tm.tm_year = 80;
	time_point<system_clock, nanoseconds> tp_1980 = std::chrono::system_clock::from_time_t(std::mktime(&tm));
	tp_1980 += std::chrono::seconds(static_cast<uint64_t>(parsed_data->gps_week * 7 * 24 * 3600 +
		parsed_data->toe)) - seconds(18);
		

	Vector3D r;
	Vector3D v;

	/*descart_from_keplerian_elements(keplerian_elements, tp_1980, epoch, r, v);
	igsk_data->x = r.x();
	igsk_data->y = r.y();
	igsk_data->z = r.z();

	igsk_data->v_x = v.x();
	igsk_data->v_y = v.y();
	igsk_data->v_z = v.z();*/

	double delta_t = (tp_1980 - epoch).count();

	double mean_anomaly = parsed_data->m_0 + corrected_mean_motion * delta_t;

	double eccentric_anomaly = mean_anomaly;
	double e_0;

	do
	{
		e_0 = eccentric_anomaly;
		eccentric_anomaly = e_0 - (e_0 - parsed_data->ecc * sin(e_0) - mean_anomaly) / 
									(1.0 - parsed_data->ecc * cos(e_0));
	} while (std::abs(eccentric_anomaly - e_0) > eps);
	
	double true_anomaly =		std::atan2(sin(eccentric_anomaly) * 
								sqrt(1.0 - parsed_data->ecc * parsed_data->ecc),
								cos(eccentric_anomaly) - parsed_data->ecc);
	if (true_anomaly < 0.0)		true_anomaly += 2.0 * M_PI;
	double arg_of_lat =			true_anomaly + parsed_data->omega;
	double arg_of_lat_corr =	parsed_data->cuc * cos(2.0 * arg_of_lat) + 
								parsed_data->cus * sin(2.0 * arg_of_lat);
	double rad_corr =			parsed_data->crc * cos(2.0 * arg_of_lat) + 
								parsed_data->crs * sin(2.0 * arg_of_lat);
	double inc_corr =			parsed_data->cic * cos(2.0 * arg_of_lat) +
								parsed_data->cis * sin(2.0 * arg_of_lat);
	double mk_dot =				mean_motion;
	double ek_dot =				mk_dot / (1.0 - parsed_data->ecc * cos(eccentric_anomaly));
	double true_anomaly_dot =	sin(eccentric_anomaly) * ek_dot *
								(1.0 + parsed_data->ecc * cos(true_anomaly)) /
								((1.0 - cos(eccentric_anomaly) * parsed_data->ecc) * sin(true_anomaly));
	double arg_of_lat_dot =		true_anomaly_dot;
	double delta_u_dot =		2.0 * (parsed_data->cus * cos(2.0 * arg_of_lat) -
								parsed_data->cuc * sin(2.0 * arg_of_lat)) * arg_of_lat_dot;
	double delta_r_dot =		2.0 * (parsed_data->crs * cos(2.0 * arg_of_lat) -
								parsed_data->crc * sin(2.0 * arg_of_lat)) * arg_of_lat_dot;
	double delta_i_dot =		2.0 * (parsed_data->cis * cos(2.0 * arg_of_lat) -
								parsed_data->cic * sin(2.0 * arg_of_lat)) * arg_of_lat_dot;
	arg_of_lat +=				arg_of_lat_corr;
	double radius =				parsed_data->sqrt_a * parsed_data->sqrt_a * 
								(1.0 - parsed_data->ecc * cos(eccentric_anomaly)) + rad_corr;
	double inclination =		parsed_data->i_0 + parsed_data->i_dot * delta_t + inc_corr;
	double orb_plane_pos_x =	radius * cos(arg_of_lat);
	double orb_plane_pos_y =	radius * sin(arg_of_lat);
	double longitude_of_ascending_node = parsed_data->omega_0 + (parsed_data->omega_dot - we) * delta_t - 
											we * parsed_data->toe;
	Vector3D not_igsk = {	orb_plane_pos_x * cos(longitude_of_ascending_node) -
							orb_plane_pos_y * sin(longitude_of_ascending_node) * cos(inclination),
							orb_plane_pos_x * sin(longitude_of_ascending_node) +
							orb_plane_pos_y * cos(longitude_of_ascending_node) * cos(inclination),
							orb_plane_pos_y * sin(inclination)};
	/*Vector3D o_t = { cos(true_anomaly), sin(true_anomaly), 0.0 };
	o_t = o_t * radius;
	Matrix3D turn_z_asc(TURN_Z, -longitude_of_ascending_node);
	Matrix3D turn_x_inc(TURN_X, -inclination);
	Matrix3D turn_z_per(TURN_Z, -(parsed_data->omega + arg_of_lat_corr));
	Matrix3D transform = turn_z_asc * turn_x_inc * turn_z_per;
	Vector3D r_t = transform * o_t;

	igsk_data->x = r_t.x();
	igsk_data->y = r_t.y();
	igsk_data->z = r_t.z();
	
	Vector3D os_t = {-sin(eccentric_anomaly), sqrt(1.0 - parsed_data->ecc * parsed_data->ecc) 
					* cos(eccentric_anomaly), 0.0};
	os_t = os_t * parsed_data->sqrt_a * sqrt(gm) / radius;
	Vector3D v_t = transform * os_t;
	igsk_data->v_x = v_t.x();
	igsk_data->v_y = v_t.y();
	igsk_data->v_z = v_t.z();*/
	std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> time_point =
		std::chrono::system_clock::from_time_t(std::mktime(&igsk_data->utc_tm));
	AstronomicTime at(epoch);
	long double tt = at.GetTT();
	long double tdb = at.GetTDB();
	AngleArguments aa(tdb, tt);

	Matrix3D transform_matrix = aa.rotation * aa.nutation * aa.precession;
	Matrix3D back_transform_matrix = -transform_matrix;
	Vector3D igsk = back_transform_matrix * not_igsk;

	igsk_data->x = igsk.x();
	igsk_data->y = igsk.y();
	igsk_data->z = igsk.z();
	
	double uk_dot =			arg_of_lat_dot + delta_u_dot;
	double rk_dot =			parsed_data->sqrt_a * parsed_data->sqrt_a * parsed_data->ecc *
							sin(eccentric_anomaly) * ek_dot + delta_r_dot;
	double ik_dot =			parsed_data->i_dot + delta_i_dot;

	double xk_dot =			rk_dot * cos(arg_of_lat) - radius * sin(arg_of_lat) * uk_dot;
	double yk_dot =			rk_dot * sin(arg_of_lat) + radius * cos(arg_of_lat) * uk_dot;
	double omegak_dot =		parsed_data->omega_dot - we;
	double vx =				xk_dot * cos(longitude_of_ascending_node) - yk_dot * cos(inclination) *
							sin(longitude_of_ascending_node) + radius * sin(arg_of_lat) *
							sin(inclination) * sin(longitude_of_ascending_node) * ik_dot -
							not_igsk.y() * omegak_dot;
	double vy =				xk_dot * sin(longitude_of_ascending_node) + yk_dot * cos(inclination) *
							cos(longitude_of_ascending_node) - radius * sin(arg_of_lat) *
							sin(inclination) * ik_dot * cos(longitude_of_ascending_node) +
							not_igsk.x() * omegak_dot;
	double vz =				yk_dot * sin(inclination) + radius * sin(arg_of_lat) * cos(inclination) *
							ik_dot;
	Vector3D v_not_igsk = { vx, vy, vz };
	Vector3D v_igsk = back_transform_matrix * v_not_igsk;

	igsk_data->v_x = v_igsk.value[0];
	igsk_data->v_y = v_igsk.value[1];
	igsk_data->v_z = v_igsk.value[2];
	
	return igsk_data;
}

std::shared_ptr<rinex::RINEX_Message> rinex::GPS_msg_factory::create(RINEX_Msg_Id msg_id, 
																	std::vector<std::uint8_t>&& data)
{
	return std::make_shared<GPS_msg>(msg_id, std::forward<std::vector<std::uint8_t>>(data));
}
