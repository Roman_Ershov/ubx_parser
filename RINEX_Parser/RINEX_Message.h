#pragma once

#include "RINEX_Msg_Id.h"
#include <vector>
#include <memory>
#include <string>

namespace rinex
{

	struct igsk_data
	{
		std::string gnss_type;
		std::uint8_t sv_id;
		std::tm utc_tm;
		double x;
		double y;
		double z;
		double v_x;
		double v_y;
		double v_z;
	};

	class RINEX_Message
	{
	protected:
		RINEX_Msg_Id msg_id_;
		std::vector <std::uint8_t> data;
		
		static double parse_double(std::vector<uint8_t>::iterator &iterator)
		{
			constexpr int dbl_len = 19;
			const auto begin_iterator = iterator;
			std::string data;
			while (std::distance(begin_iterator, iterator) < dbl_len)
			{
				data.push_back(*iterator++);
			}

			auto pos_d = data.find(std::string("D"));
			if (pos_d != std::string::npos)
			{
				data.replace(pos_d, 1, std::string("e"));
			}
			return std::stod(data);
		}

	public:

		RINEX_Message(RINEX_Msg_Id msg_id,
			std::vector<std::uint8_t>&& data) :
			msg_id_(msg_id),
			data(data)
		{}

		RINEX_Msg_Id getType()
		{
			return msg_id_;
		}

		RINEX_Message() = default;
		RINEX_Message(const RINEX_Message&) = default;
		RINEX_Message(RINEX_Message&&) = default;
		RINEX_Message& operator=(const RINEX_Message&) = default;
		RINEX_Message& operator=(RINEX_Message&&) = default;

		virtual ~RINEX_Message() = default;
		virtual void do_parse() = 0;
		virtual std::shared_ptr<igsk_data> to_igsk() = 0;
	};
}


