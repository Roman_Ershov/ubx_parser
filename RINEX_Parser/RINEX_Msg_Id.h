#pragma once

#include <cstdint>

namespace rinex
{

	enum class RINEX_Msg_Id
	{
		GPS = 0,
		GLONASS = 6
	};

}
