//
// Created by yelowt on 24.10.17.
//

#ifndef BNO_ANGLEARGUMENTS_H
#define BNO_ANGLEARGUMENTS_H

#include "VectorMatrix3D.h"

/** Угловые аргументы.*/
struct AngleArguments
{
    /** Конструктор с параметрами.
        @param t_dbc динамическое барицентрическое время.
        @param UT1 всемирное время.*/
    AngleArguments(long double t_dbc, long double UT1);

    /** Угол наклона мгновенной эклиптики к среднему подвижному экватору.*/
    double eps;

    /** Матрица нутации.*/
    Matrix3D nutation;

    /** Матрица прецессии.*/
    Matrix3D precession;

    /** Матрица вращения Земли.*/
    Matrix3D rotation;

    /** Смещение от эпохи J2000.*/
    double t_c;

    /** Средняя долгота Луны.*/
    double lambda;

    /** Средняя аномалия Луны.*/
    double l;

    /** Средняя аномалия Солнца.*/
    double ls;

    /** Средний аргумент широты Луны.*/
    double F;

    /** Разность средних долгот Луны и Солнца.*/
    double D;

    /** */
    double rg;

    /** Число для перевода дуговых секунд в радианную меру.*/
    double rs;
private:
    void Calculate(long double t_dbc, long double UT1);
};


#endif //BNO_ANGLEARGUMENTS_H
