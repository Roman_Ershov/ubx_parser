#include "RINEX_Parser.h"
#include "Message_type_factory.h"
#include "InitConditions.hxx"

RINEX_Parser::RINEX_Parser(const std::string& file)
{
	rnx_file.open(file.c_str());
}

void RINEX_Parser::do_parse()
{
	std::istreambuf_iterator<uint8_t> istreambuf_iterator(rnx_file);
	const std::istreambuf_iterator<uint8_t> eof;

	std::unique_ptr<rinex::Message_type_factory> message_type_factory = std::make_unique<rinex::Message_type_factory>();
	message_type_factory->register_all_factories();

	while (istreambuf_iterator != eof)
	{
		uint8_t msg_type = *istreambuf_iterator;
		rinex::RINEX_Msg_Id msg_id;
		if (msg_type == 'R') msg_id = rinex::RINEX_Msg_Id::GLONASS;
		else if (msg_type == 'G') msg_id = rinex::RINEX_Msg_Id::GPS;
		else msg_id = rinex::RINEX_Msg_Id::GPS;

		size_t count_strs = 1;
		size_t msg_len = msg_id == rinex::RINEX_Msg_Id::GLONASS ? 4 : 8;
		std::vector<uint8_t> msg_data;
		while (count_strs < msg_len || (*istreambuf_iterator != '\n'))
		{	
			if (*istreambuf_iterator == '\n') ++count_strs;
			msg_data.emplace_back(*(istreambuf_iterator++));
			if (istreambuf_iterator == eof) break;
		}

		auto msg = message_type_factory->create(msg_id, std::move(msg_data));
		if (msg != nullptr) {
			msg->do_parse();
			messages.emplace_back(msg);
		}

		if (istreambuf_iterator != eof) ++istreambuf_iterator;
	}
}

void RINEX_Parser::export_to_xml()
{
	std::vector<std::shared_ptr<rinex::igsk_data>> igsk_data;

	for (const auto& message : messages)
	{
		igsk_data.emplace_back(message->to_igsk());
	}

	try
	{
		Sat_Data::sat_sequence sat_sequence;

		for (auto igsk : igsk_data)
		{
			xml_schema::date_time date_time(igsk->utc_tm.tm_year + 1900,
											igsk->utc_tm.tm_mon + 1,
											igsk->utc_tm.tm_mday,
											igsk->utc_tm.tm_hour,
											igsk->utc_tm.tm_min,
											igsk->utc_tm.tm_sec);
			SatType sat;
			sat.sat_type(igsk->gnss_type);
			sat.sat_num(igsk->sv_id);
			sat.time(date_time);
			sat.x(igsk->x);
			sat.y(igsk->y);
			sat.z(igsk->z);
			sat.v_x(igsk->v_x);
			sat.v_y(igsk->v_y);
			sat.v_z(igsk->v_z);

			sat_sequence.push_back(sat);
		}
		auto sat_data = std::make_shared<Sat_Data>();
		sat_data->sat(sat_sequence);

		xml_schema::namespace_infomap map;
		map[""].name = "";
		map[""].schema = "InitConditions.xsd";
		std::ofstream ofs("InitConditions.xml");
		Sat_Data_(ofs, *sat_data, map);
	}
	catch (const xml_schema::exception &e)
	{

	}
}

RINEX_Parser::~RINEX_Parser()
{
	rnx_file.close();
}