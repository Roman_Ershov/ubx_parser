#include "Rxm_rawx_msg.h"

ublox::Rxm_rawx_msg::Rxm_rawx_msg(UBX_MsgId msg_id,
	std::uint16_t length,
	std::uint16_t checksum,
	std::vector<std::uint8_t>&& data) :

	UBX_Message(msg_id, length, checksum, std::forward<std::vector<std::uint8_t>>(data))
{
	parsed_data = std::make_shared<Rxm_rawx_data>();
}

std::shared_ptr<ublox::UBX_Message> ublox::Rxm_rawx_msg_factory::create(UBX_MsgId msg_id,
	std::uint16_t length,
	std::uint16_t checksum,
	std::vector<std::uint8_t>&& data)
{
	return std::make_shared<Rxm_rawx_msg>(msg_id, length, checksum, std::forward<std::vector<std::uint8_t>>(data));
}

void ublox::Rxm_rawx_msg::do_parse()
{
	size_t offset = 0;
	std::memcpy(&parsed_data->rcvTow, data.data(), sizeof(parsed_data->rcvTow));
	offset += sizeof(parsed_data->rcvTow);
	std::memcpy(&parsed_data->week, data.data() + offset, sizeof(parsed_data->week));
	offset += sizeof(parsed_data->week);
	std::memcpy(&parsed_data->leapS, data.data() + offset, sizeof(parsed_data->leapS));
	offset += sizeof(parsed_data->leapS);
	std::memcpy(&parsed_data->numMeas, data.data() + offset, sizeof(parsed_data->numMeas));
	offset += sizeof(parsed_data->numMeas);
	std::uint8_t recStat;
	std::memcpy(&recStat, data.data() + offset, sizeof(recStat));
	offset += sizeof(recStat);
	parsed_data->leapSec = recStat & 0x02;
	parsed_data->clkReset = recStat & 0x01;

	offset += 3 * sizeof(std::uint8_t);  ///// reserver 3 uchar

	for (std::uint8_t meas = 0; meas < parsed_data->numMeas; meas++)
	{
		Rxm_rawx_meas_data meas_data;

		std::memcpy(&meas_data.prMes, data.data() + offset, sizeof(meas_data.prMes));
		offset += sizeof(meas_data.prMes);

		std::memcpy(&meas_data.cpMes, data.data() + offset, sizeof(meas_data.cpMes));
		offset += sizeof(meas_data.cpMes);

		std::memcpy(&meas_data.doMes, data.data() + offset, sizeof(meas_data.doMes));
		offset += sizeof(meas_data.doMes);

		std::memcpy(&meas_data.gnssId, data.data() + offset, sizeof(meas_data.gnssId));
		offset += sizeof(meas_data.gnssId);

		std::memcpy(&meas_data.svId, data.data() + offset, sizeof(meas_data.svId));
		offset += sizeof(meas_data.svId);

		offset += sizeof(uint8_t);   ///// reserved byte

		std::memcpy(&meas_data.freqId, data.data() + offset, sizeof(meas_data.freqId));
		offset += sizeof(meas_data.freqId);

		std::memcpy(&meas_data.lockTime, data.data() + offset, sizeof(meas_data.lockTime));
		offset += sizeof(meas_data.lockTime);

		std::memcpy(&meas_data.cno, data.data() + offset, sizeof(meas_data.cno));
		offset += sizeof(meas_data.cno);

		uint8_t prStdev_;
		std::memcpy(&prStdev_, data.data() + offset, sizeof(prStdev_));
		offset += sizeof(prStdev_);
		meas_data.prStdev = 0.01 * (1 << (prStdev_ & 0x0f));

		uint8_t cpStdev_;
		std::memcpy(&cpStdev_, data.data() + offset, sizeof(cpStdev_));
		offset += sizeof(cpStdev_);
		meas_data.cpStdev = 0.004 *  (cpStdev_ & 0x0f);

		uint8_t doStdev_;
		std::memcpy(&doStdev_, data.data() + offset, sizeof(doStdev_));
		offset += sizeof(doStdev_);
		meas_data.doStdev = 0.002 * (1 << (doStdev_ & 0x0f));

		uint8_t trkStat_;
		std::memcpy(&trkStat_, data.data() + offset, sizeof(trkStat_));
		offset += sizeof(trkStat_);
		meas_data.prValid = trkStat_ & 0x01;
		meas_data.cpValid = trkStat_ & 0x02;
		meas_data.halfCyc = trkStat_ & 0x04;
		meas_data.subHalfCyc = trkStat_ & 0x08;

		offset++;

		parsed_data->meas_data.emplace_back(meas_data);
	}
}

std::shared_ptr<ublox::Rxm_rawx_data> ublox::Rxm_rawx_msg::get_parsed_data()
{
	return parsed_data;
}
