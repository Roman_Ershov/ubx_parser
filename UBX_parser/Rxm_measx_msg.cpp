#include "Rxm_measx_msg.h"

ublox::Rxm_measx_msg::Rxm_measx_msg(	UBX_MsgId msg_id, 
										std::uint16_t length, 
										std::uint16_t checksum,
										std::vector<std::uint8_t>&& data):
		
										UBX_Message(msg_id, 
													length,
													checksum, 
													std::forward<std::vector<std::uint8_t>>(data))
{
	parsed_data = std::make_shared<Rxm_measx_data>();
}

void ublox::Rxm_measx_msg::do_parse()
{
	size_t offset = 0;
	std::memcpy(&parsed_data->version, data.data() + offset, sizeof(parsed_data->version));
	offset += sizeof(parsed_data->version);
	offset += 3 * sizeof(uint8_t);

	std::memcpy(&parsed_data->gpsTOW, data.data() + offset, sizeof(parsed_data->gpsTOW));
	offset += sizeof(parsed_data->gpsTOW);

	std::memcpy(&parsed_data->gloTOW, data.data() + offset, sizeof(parsed_data->gloTOW));
	offset += sizeof(parsed_data->gloTOW);

	std::memcpy(&parsed_data->bdsTOW, data.data() + offset, sizeof(parsed_data->bdsTOW));
	offset += sizeof(parsed_data->bdsTOW);
	offset += 4 * sizeof(uint8_t);

	std::memcpy(&parsed_data->qzssTOW, data.data() + offset, sizeof(parsed_data->qzssTOW));
	offset += sizeof(parsed_data->qzssTOW);

	std::uint16_t gpsTOWAcc_;
	std::memcpy(&gpsTOWAcc_, data.data() + offset, sizeof(gpsTOWAcc_));
	offset += sizeof(gpsTOWAcc_);
	parsed_data->gpsTOWAcc = 0.0625 * gpsTOWAcc_;

	std::uint16_t gloTOWAcc_;
	std::memcpy(&gloTOWAcc_, data.data() + offset, sizeof(gloTOWAcc_));
	offset += sizeof(gloTOWAcc_);
	parsed_data->gloTOWAcc = 0.0625 * gloTOWAcc_;

	std::uint16_t bdsTOWAcc_;
	std::memcpy(&bdsTOWAcc_, data.data() + offset, sizeof(bdsTOWAcc_));
	offset += sizeof(bdsTOWAcc_);
	parsed_data->bdsTOWAcc = 0.0625 * bdsTOWAcc_;

	offset += 2 * sizeof(std::uint8_t);
	std::uint16_t qzssTOWAcc_;
	std::memcpy(&qzssTOWAcc_, data.data() + offset, sizeof(qzssTOWAcc_));
	offset += sizeof(qzssTOWAcc_);
	parsed_data->qzssTOWAcc = 0.0625 * qzssTOWAcc_;

	std::memcpy(&parsed_data->numSV, data.data() + offset, sizeof(parsed_data->numSV));
	offset += sizeof(parsed_data->numSV);

	std::uint8_t flags_;
	std::memcpy(&flags_, data.data() + offset, sizeof(flags_));
	offset += sizeof(flags_);
	parsed_data->towSet = flags_ & 0x03;

	offset += 8 * sizeof(uint8_t);

	for (auto sat = 0; sat < parsed_data->numSV; sat++)
	{
		Rxm_measx_sat_data sat_data;

		std::memcpy(&sat_data.gnssId, data.data() + offset, sizeof(sat_data.gnssId));
		offset += sizeof(sat_data.gnssId);

		std::memcpy(&sat_data.svId, data.data() + offset, sizeof(sat_data.svId));
		offset += sizeof(sat_data.svId);

		std::memcpy(&sat_data.cNo, data.data() + offset, sizeof(sat_data.cNo));
		offset += sizeof(sat_data.cNo);

		std::memcpy(&sat_data.mpathIndic, data.data() + offset, sizeof(sat_data.mpathIndic));
		offset += sizeof(sat_data.mpathIndic);

		std::int32_t dopplerMS_;
		std::memcpy(&dopplerMS_, data.data() + offset, sizeof(dopplerMS_));
		offset += sizeof(dopplerMS_);
		sat_data.dopplesMS = 0.04 * dopplerMS_;

		std::int32_t dopplerHz_;
		std::memcpy(&dopplerHz_, data.data() + offset, sizeof(dopplerHz_));
		offset += sizeof(dopplerHz_);
		sat_data.dopplerHz = 0.2 * dopplerHz_;

		std::memcpy(&sat_data.wholeChips, data.data() + offset, sizeof(sat_data.wholeChips));
		offset += sizeof(sat_data.wholeChips);

		std::memcpy(&sat_data.fracChips, data.data() + offset, sizeof(sat_data.fracChips));
		offset += sizeof(sat_data.fracChips);

		std::uint32_t codePhase_;
		std::memcpy(&codePhase_, data.data() + offset, sizeof(codePhase_));
		offset += sizeof(codePhase_);
		sat_data.codePhase = pow(2.0, -21.0) * codePhase_;

		std::memcpy(&sat_data.intCodePhase, data.data() + offset, sizeof(sat_data.intCodePhase));
		offset += sizeof(sat_data.intCodePhase);

		std::memcpy(&sat_data.pseuRangeRMESrr, data.data() + offset, sizeof(sat_data.pseuRangeRMESrr));
		offset += sizeof(sat_data.pseuRangeRMESrr);

		offset += 2 * sizeof(uint8_t);

		parsed_data->sat_data.emplace_back(sat_data);
	}
}

std::shared_ptr<ublox::Rxm_measx_data> ublox::Rxm_measx_msg::get_parsed_data()
{
	return parsed_data;
}


std::shared_ptr<ublox::UBX_Message> ublox::Rxm_measx_msg_factory::create(UBX_MsgId msg_id, std::uint16_t length,
	std::uint16_t checksum, std::vector<std::uint8_t>&& data)
{
	return std::make_shared<Rxm_measx_msg>(msg_id, length, checksum, std::forward < std::vector<std::uint8_t> >(data));
}
