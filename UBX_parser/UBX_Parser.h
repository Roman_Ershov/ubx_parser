#pragma once
#include <string>
#include <fstream>
#include <vector>
#include "UBX_Message.h"

class UBX_Parser
{
	std::basic_ifstream<uint8_t> ubx_file;

	std::vector<std::shared_ptr<ublox::UBX_Message>> messages;

	static uint16_t to_big_endian(uint8_t byte_1, uint8_t byte_2);

public:

	explicit UBX_Parser(const std::string& file);

	void do_parse();

	void export_to_xml();

	void export_to_xml_peleng();

	virtual ~UBX_Parser();
};
