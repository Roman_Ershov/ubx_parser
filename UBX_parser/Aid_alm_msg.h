#pragma once
#include "UBX_Message.h"
#include "Message_factory.h"

namespace ublox
{
	class Aid_alm_msg :  public UBX_Message
	{
	public:
		Aid_alm_msg(UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data);

		void do_parse() override;
	};

	class Aid_alm_msg_factory : public Message_factory
	{
		std::shared_ptr<UBX_Message> create(UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data) override;
	};
}
