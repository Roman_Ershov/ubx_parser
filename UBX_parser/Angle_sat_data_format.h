﻿#pragma once

#using <mscorlib.dll>
#using <System.dll>
#using <System.Data.dll>
#using <System.Xml.dll>

using namespace System::Security::Permissions;
[assembly:SecurityPermissionAttribute(SecurityAction::RequestMinimum, SkipVerification=false)];
// 
// Этот исходный код был создан с помощью xsd, версия=4.6.1055.0.
// 
namespace UBX_parser {
    using namespace System;
    ref class Angle_sat_Data;
    
    
    /// <summary>
///Represents a strongly typed in-memory cache of data.
///</summary>
    [System::Serializable, 
    System::ComponentModel::DesignerCategoryAttribute(L"code"), 
    System::ComponentModel::ToolboxItem(true), 
    System::Xml::Serialization::XmlSchemaProviderAttribute(L"GetTypedDataSetSchema"), 
    System::Xml::Serialization::XmlRootAttribute(L"Angle_sat_Data"), 
    System::ComponentModel::Design::HelpKeywordAttribute(L"vs.data.DataSet")]
    public ref class Angle_sat_Data : public ::System::Data::DataSet {
        public : ref class satDataTable;
        public : ref class satRow;
        public : ref class satRowChangeEvent;
        
        private: UBX_parser::Angle_sat_Data::satDataTable^  tablesat;
        
        private: ::System::Data::SchemaSerializationMode _schemaSerializationMode;
        
        public : [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        delegate System::Void satRowChangeEventHandler(::System::Object^  sender, UBX_parser::Angle_sat_Data::satRowChangeEvent^  e);
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        Angle_sat_Data();
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        Angle_sat_Data(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context);
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::Browsable(false), 
        System::ComponentModel::DesignerSerializationVisibility(::System::ComponentModel::DesignerSerializationVisibility::Content)]
        property UBX_parser::Angle_sat_Data::satDataTable^  sat {
            UBX_parser::Angle_sat_Data::satDataTable^  get();
        }
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::BrowsableAttribute(true), 
        System::ComponentModel::DesignerSerializationVisibilityAttribute(::System::ComponentModel::DesignerSerializationVisibility::Visible)]
        virtual property ::System::Data::SchemaSerializationMode SchemaSerializationMode {
            ::System::Data::SchemaSerializationMode get() override;
            System::Void set(::System::Data::SchemaSerializationMode value) override;
        }
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::DesignerSerializationVisibilityAttribute(::System::ComponentModel::DesignerSerializationVisibility::Hidden)]
        property ::System::Data::DataTableCollection^  Tables {
            ::System::Data::DataTableCollection^  get() new;
        }
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
        System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
        System::ComponentModel::DesignerSerializationVisibilityAttribute(::System::ComponentModel::DesignerSerializationVisibility::Hidden)]
        property ::System::Data::DataRelationCollection^  Relations {
            ::System::Data::DataRelationCollection^  get() new;
        }
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Void InitializeDerivedDataSet() override;
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Data::DataSet^  Clone() override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Boolean ShouldSerializeTables() override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Boolean ShouldSerializeRelations() override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Void ReadXmlSerializable(::System::Xml::XmlReader^  reader) override;
        
        protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        virtual ::System::Xml::Schema::XmlSchema^  GetSchemaSerializable() override;
        
        internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void InitVars();
        
        internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void InitVars(::System::Boolean initTable);
        
        private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void InitClass();
        
        private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Boolean ShouldSerializesat();
        
        private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ::System::Void SchemaChanged(::System::Object^  sender, ::System::ComponentModel::CollectionChangeEventArgs^  e);
        
        public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        static ::System::Xml::Schema::XmlSchemaComplexType^  GetTypedDataSetSchema(::System::Xml::Schema::XmlSchemaSet^  xs);
        
        public : /// <summary>
///Represents the strongly named DataTable class.
///</summary>
        [System::Serializable, 
        System::Xml::Serialization::XmlSchemaProviderAttribute(L"GetTypedTableSchema")]
        ref class satDataTable : public ::System::Data::DataTable, public ::System::Collections::IEnumerable {
            
            private: ::System::Data::DataColumn^  columntime;
            
            private: ::System::Data::DataColumn^  columnsat_num;
            
            private: ::System::Data::DataColumn^  columnsat_type;
            
            private: ::System::Data::DataColumn^  columnazimuth;
            
            private: ::System::Data::DataColumn^  columnelevation;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event UBX_parser::Angle_sat_Data::satRowChangeEventHandler^  satRowChanging;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event UBX_parser::Angle_sat_Data::satRowChangeEventHandler^  satRowChanged;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event UBX_parser::Angle_sat_Data::satRowChangeEventHandler^  satRowDeleting;
            
            public: [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            event UBX_parser::Angle_sat_Data::satRowChangeEventHandler^  satRowDeleted;
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satDataTable();
            internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satDataTable(::System::Data::DataTable^  table);
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satDataTable(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context);
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  timeColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  sat_numColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  sat_typeColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  azimuthColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataColumn^  elevationColumn {
                ::System::Data::DataColumn^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0"), 
            System::ComponentModel::Browsable(false)]
            property ::System::Int32 Count {
                ::System::Int32 get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property UBX_parser::Angle_sat_Data::satRow^  default [::System::Int32 ] {
                UBX_parser::Angle_sat_Data::satRow^  get(::System::Int32 index);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void AddsatRow(UBX_parser::Angle_sat_Data::satRow^  row);
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            UBX_parser::Angle_sat_Data::satRow^  AddsatRow(System::DateTime time, System::UInt32 sat_num, System::String^  sat_type, 
                        System::Int64 azimuth, System::Int64 elevation);
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Collections::IEnumerator^  GetEnumerator();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Data::DataTable^  Clone() override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Data::DataTable^  CreateInstance() override;
            
            internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void InitVars();
            
            private: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void InitClass();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            UBX_parser::Angle_sat_Data::satRow^  NewsatRow();
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Data::DataRow^  NewRowFromBuilder(::System::Data::DataRowBuilder^  builder) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Type^  GetRowType() override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowChanged(::System::Data::DataRowChangeEventArgs^  e) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowChanging(::System::Data::DataRowChangeEventArgs^  e) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowDeleted(::System::Data::DataRowChangeEventArgs^  e) override;
            
            protected: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            virtual ::System::Void OnRowDeleting(::System::Data::DataRowChangeEventArgs^  e) override;
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void RemovesatRow(UBX_parser::Angle_sat_Data::satRow^  row);
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            static ::System::Xml::Schema::XmlSchemaComplexType^  GetTypedTableSchema(::System::Xml::Schema::XmlSchemaSet^  xs);
        };
        
        public : /// <summary>
///Represents strongly named DataRow class.
///</summary>
        ref class satRow : public ::System::Data::DataRow {
            
            private: UBX_parser::Angle_sat_Data::satDataTable^  tablesat;
            
            internal: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satRow(::System::Data::DataRowBuilder^  rb);
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::DateTime time {
                System::DateTime get();
                System::Void set(System::DateTime value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::UInt32 sat_num {
                System::UInt32 get();
                System::Void set(System::UInt32 value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::String^  sat_type {
                System::String^  get();
                System::Void set(System::String^  value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Int64 azimuth {
                System::Int64 get();
                System::Void set(System::Int64 value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property System::Int64 elevation {
                System::Int64 get();
                System::Void set(System::Int64 value);
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean IstimeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void SettimeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean Issat_numNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void Setsat_numNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean Issat_typeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void Setsat_typeNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean IsazimuthNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void SetazimuthNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Boolean IselevationNull();
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            ::System::Void SetelevationNull();
        };
        
        public : /// <summary>
///Row event argument class
///</summary>
        [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
        ref class satRowChangeEvent : public ::System::EventArgs {
            
            private: UBX_parser::Angle_sat_Data::satRow^  eventRow;
            
            private: ::System::Data::DataRowAction eventAction;
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute]
            [System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            satRowChangeEvent(UBX_parser::Angle_sat_Data::satRow^  row, ::System::Data::DataRowAction action);
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property UBX_parser::Angle_sat_Data::satRow^  Row {
                UBX_parser::Angle_sat_Data::satRow^  get();
            }
            
            public: [System::Diagnostics::DebuggerNonUserCodeAttribute, 
            System::CodeDom::Compiler::GeneratedCodeAttribute(L"System.Data.Design.TypedDataSetGenerator", L"4.0.0.0")]
            property ::System::Data::DataRowAction Action {
                ::System::Data::DataRowAction get();
            }
        };
    };
}
namespace UBX_parser {
    
    
    inline Angle_sat_Data::Angle_sat_Data() {
        this->BeginInit();
        this->InitClass();
        ::System::ComponentModel::CollectionChangeEventHandler^  schemaChangedHandler = gcnew ::System::ComponentModel::CollectionChangeEventHandler(this, &UBX_parser::Angle_sat_Data::SchemaChanged);
        __super::Tables->CollectionChanged += schemaChangedHandler;
        __super::Relations->CollectionChanged += schemaChangedHandler;
        this->EndInit();
    }
    
    inline Angle_sat_Data::Angle_sat_Data(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context) : 
            ::System::Data::DataSet(info, context, false) {
        if (this->IsBinarySerialized(info, context) == true) {
            this->InitVars(false);
            ::System::ComponentModel::CollectionChangeEventHandler^  schemaChangedHandler1 = gcnew ::System::ComponentModel::CollectionChangeEventHandler(this, &UBX_parser::Angle_sat_Data::SchemaChanged);
            this->Tables->CollectionChanged += schemaChangedHandler1;
            this->Relations->CollectionChanged += schemaChangedHandler1;
            return;
        }
        ::System::String^  strSchema = (cli::safe_cast<::System::String^  >(info->GetValue(L"XmlSchema", ::System::String::typeid)));
        if (this->DetermineSchemaSerializationMode(info, context) == ::System::Data::SchemaSerializationMode::IncludeSchema) {
            ::System::Data::DataSet^  ds = (gcnew ::System::Data::DataSet());
            ds->ReadXmlSchema((gcnew ::System::Xml::XmlTextReader((gcnew ::System::IO::StringReader(strSchema)))));
            if (ds->Tables[L"sat"] != nullptr) {
                __super::Tables->Add((gcnew UBX_parser::Angle_sat_Data::satDataTable(ds->Tables[L"sat"])));
            }
            this->DataSetName = ds->DataSetName;
            this->Prefix = ds->Prefix;
            this->Namespace = ds->Namespace;
            this->Locale = ds->Locale;
            this->CaseSensitive = ds->CaseSensitive;
            this->EnforceConstraints = ds->EnforceConstraints;
            this->Merge(ds, false, ::System::Data::MissingSchemaAction::Add);
            this->InitVars();
        }
        else {
            this->ReadXmlSchema((gcnew ::System::Xml::XmlTextReader((gcnew ::System::IO::StringReader(strSchema)))));
        }
        this->GetSerializationData(info, context);
        ::System::ComponentModel::CollectionChangeEventHandler^  schemaChangedHandler = gcnew ::System::ComponentModel::CollectionChangeEventHandler(this, &UBX_parser::Angle_sat_Data::SchemaChanged);
        __super::Tables->CollectionChanged += schemaChangedHandler;
        this->Relations->CollectionChanged += schemaChangedHandler;
    }
    
    inline UBX_parser::Angle_sat_Data::satDataTable^  Angle_sat_Data::sat::get() {
        return this->tablesat;
    }
    
    inline ::System::Data::SchemaSerializationMode Angle_sat_Data::SchemaSerializationMode::get() {
        return this->_schemaSerializationMode;
    }
    inline System::Void Angle_sat_Data::SchemaSerializationMode::set(::System::Data::SchemaSerializationMode value) {
        this->_schemaSerializationMode = __identifier(value);
    }
    
    inline ::System::Data::DataTableCollection^  Angle_sat_Data::Tables::get() {
        return __super::Tables;
    }
    
    inline ::System::Data::DataRelationCollection^  Angle_sat_Data::Relations::get() {
        return __super::Relations;
    }
    
    inline ::System::Void Angle_sat_Data::InitializeDerivedDataSet() {
        this->BeginInit();
        this->InitClass();
        this->EndInit();
    }
    
    inline ::System::Data::DataSet^  Angle_sat_Data::Clone() {
        UBX_parser::Angle_sat_Data^  cln = (cli::safe_cast<UBX_parser::Angle_sat_Data^  >(__super::Clone()));
        cln->InitVars();
        cln->SchemaSerializationMode = this->SchemaSerializationMode;
        return cln;
    }
    
    inline ::System::Boolean Angle_sat_Data::ShouldSerializeTables() {
        return false;
    }
    
    inline ::System::Boolean Angle_sat_Data::ShouldSerializeRelations() {
        return false;
    }
    
    inline ::System::Void Angle_sat_Data::ReadXmlSerializable(::System::Xml::XmlReader^  reader) {
        if (this->DetermineSchemaSerializationMode(reader) == ::System::Data::SchemaSerializationMode::IncludeSchema) {
            this->Reset();
            ::System::Data::DataSet^  ds = (gcnew ::System::Data::DataSet());
            ds->ReadXml(reader);
            if (ds->Tables[L"sat"] != nullptr) {
                __super::Tables->Add((gcnew UBX_parser::Angle_sat_Data::satDataTable(ds->Tables[L"sat"])));
            }
            this->DataSetName = ds->DataSetName;
            this->Prefix = ds->Prefix;
            this->Namespace = ds->Namespace;
            this->Locale = ds->Locale;
            this->CaseSensitive = ds->CaseSensitive;
            this->EnforceConstraints = ds->EnforceConstraints;
            this->Merge(ds, false, ::System::Data::MissingSchemaAction::Add);
            this->InitVars();
        }
        else {
            this->ReadXml(reader);
            this->InitVars();
        }
    }
    
    inline ::System::Xml::Schema::XmlSchema^  Angle_sat_Data::GetSchemaSerializable() {
        ::System::IO::MemoryStream^  stream = (gcnew ::System::IO::MemoryStream());
        this->WriteXmlSchema((gcnew ::System::Xml::XmlTextWriter(stream, nullptr)));
        stream->Position = 0;
        return ::System::Xml::Schema::XmlSchema::Read((gcnew ::System::Xml::XmlTextReader(stream)), nullptr);
    }
    
    inline ::System::Void Angle_sat_Data::InitVars() {
        this->InitVars(true);
    }
    
    inline ::System::Void Angle_sat_Data::InitVars(::System::Boolean initTable) {
        this->tablesat = (cli::safe_cast<UBX_parser::Angle_sat_Data::satDataTable^  >(__super::Tables[L"sat"]));
        if (initTable == true) {
            if (this->tablesat != nullptr) {
                this->tablesat->InitVars();
            }
        }
    }
    
    inline ::System::Void Angle_sat_Data::InitClass() {
        this->DataSetName = L"Angle_sat_Data";
        this->Prefix = L"";
        this->Namespace = L"urn:angle_sat";
        this->EnforceConstraints = true;
        this->SchemaSerializationMode = ::System::Data::SchemaSerializationMode::IncludeSchema;
        this->tablesat = (gcnew UBX_parser::Angle_sat_Data::satDataTable());
        __super::Tables->Add(this->tablesat);
    }
    
    inline ::System::Boolean Angle_sat_Data::ShouldSerializesat() {
        return false;
    }
    
    inline ::System::Void Angle_sat_Data::SchemaChanged(::System::Object^  sender, ::System::ComponentModel::CollectionChangeEventArgs^  e) {
        if (e->Action == ::System::ComponentModel::CollectionChangeAction::Remove) {
            this->InitVars();
        }
    }
    
    inline ::System::Xml::Schema::XmlSchemaComplexType^  Angle_sat_Data::GetTypedDataSetSchema(::System::Xml::Schema::XmlSchemaSet^  xs) {
        UBX_parser::Angle_sat_Data^  ds = (gcnew UBX_parser::Angle_sat_Data());
        ::System::Xml::Schema::XmlSchemaComplexType^  type = (gcnew ::System::Xml::Schema::XmlSchemaComplexType());
        ::System::Xml::Schema::XmlSchemaSequence^  sequence = (gcnew ::System::Xml::Schema::XmlSchemaSequence());
        ::System::Xml::Schema::XmlSchemaAny^  any = (gcnew ::System::Xml::Schema::XmlSchemaAny());
        any->Namespace = ds->Namespace;
        sequence->Items->Add(any);
        type->Particle = sequence;
        ::System::Xml::Schema::XmlSchema^  dsSchema = ds->GetSchemaSerializable();
        if (xs->Contains(dsSchema->TargetNamespace)) {
            ::System::IO::MemoryStream^  s1 = (gcnew ::System::IO::MemoryStream());
            ::System::IO::MemoryStream^  s2 = (gcnew ::System::IO::MemoryStream());
            try {
                ::System::Xml::Schema::XmlSchema^  schema = nullptr;
                dsSchema->Write(s1);
                for (                ::System::Collections::IEnumerator^  schemas = xs->Schemas(dsSchema->TargetNamespace)->GetEnumerator(); schemas->MoveNext();                 ) {
                    schema = (cli::safe_cast<::System::Xml::Schema::XmlSchema^  >(schemas->Current));
                    s2->SetLength(0);
                    schema->Write(s2);
                    if (s1->Length == s2->Length) {
                        s1->Position = 0;
                        s2->Position = 0;
                        for (                        ; ((s1->Position != s1->Length) 
                                    && (s1->ReadByte() == s2->ReadByte()));                         ) {
                            ;
                        }
                        if (s1->Position == s1->Length) {
                            return type;
                        }
                    }
                }
            }
            finally {
                if (s1 != nullptr) {
                    s1->Close();
                }
                if (s2 != nullptr) {
                    s2->Close();
                }
            }
        }
        xs->Add(dsSchema);
        return type;
    }
    
    
    inline Angle_sat_Data::satDataTable::satDataTable() {
        this->TableName = L"sat";
        this->BeginInit();
        this->InitClass();
        this->EndInit();
    }
    
    inline Angle_sat_Data::satDataTable::satDataTable(::System::Data::DataTable^  table) {
        this->TableName = table->TableName;
        if (table->CaseSensitive != table->DataSet->CaseSensitive) {
            this->CaseSensitive = table->CaseSensitive;
        }
        if (table->Locale->ToString() != table->DataSet->Locale->ToString()) {
            this->Locale = table->Locale;
        }
        if (table->Namespace != table->DataSet->Namespace) {
            this->Namespace = table->Namespace;
        }
        this->Prefix = table->Prefix;
        this->MinimumCapacity = table->MinimumCapacity;
    }
    
    inline Angle_sat_Data::satDataTable::satDataTable(::System::Runtime::Serialization::SerializationInfo^  info, ::System::Runtime::Serialization::StreamingContext context) : 
            ::System::Data::DataTable(info, context) {
        this->InitVars();
    }
    
    inline ::System::Data::DataColumn^  Angle_sat_Data::satDataTable::timeColumn::get() {
        return this->columntime;
    }
    
    inline ::System::Data::DataColumn^  Angle_sat_Data::satDataTable::sat_numColumn::get() {
        return this->columnsat_num;
    }
    
    inline ::System::Data::DataColumn^  Angle_sat_Data::satDataTable::sat_typeColumn::get() {
        return this->columnsat_type;
    }
    
    inline ::System::Data::DataColumn^  Angle_sat_Data::satDataTable::azimuthColumn::get() {
        return this->columnazimuth;
    }
    
    inline ::System::Data::DataColumn^  Angle_sat_Data::satDataTable::elevationColumn::get() {
        return this->columnelevation;
    }
    
    inline ::System::Int32 Angle_sat_Data::satDataTable::Count::get() {
        return this->Rows->Count;
    }
    
    inline UBX_parser::Angle_sat_Data::satRow^  Angle_sat_Data::satDataTable::default::get(::System::Int32 index) {
        return (cli::safe_cast<UBX_parser::Angle_sat_Data::satRow^  >(this->Rows[index]));
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::AddsatRow(UBX_parser::Angle_sat_Data::satRow^  row) {
        this->Rows->Add(row);
    }
    
    inline UBX_parser::Angle_sat_Data::satRow^  Angle_sat_Data::satDataTable::AddsatRow(System::DateTime time, System::UInt32 sat_num, 
                System::String^  sat_type, System::Int64 azimuth, System::Int64 elevation) {
        UBX_parser::Angle_sat_Data::satRow^  rowsatRow = (cli::safe_cast<UBX_parser::Angle_sat_Data::satRow^  >(this->NewRow()));
        cli::array< ::System::Object^  >^  columnValuesArray = gcnew cli::array< ::System::Object^  >(5) {time, sat_num, sat_type, 
            azimuth, elevation};
        rowsatRow->ItemArray = columnValuesArray;
        this->Rows->Add(rowsatRow);
        return rowsatRow;
    }
    
    inline ::System::Collections::IEnumerator^  Angle_sat_Data::satDataTable::GetEnumerator() {
        return this->Rows->GetEnumerator();
    }
    
    inline ::System::Data::DataTable^  Angle_sat_Data::satDataTable::Clone() {
        UBX_parser::Angle_sat_Data::satDataTable^  cln = (cli::safe_cast<UBX_parser::Angle_sat_Data::satDataTable^  >(__super::Clone()));
        cln->InitVars();
        return cln;
    }
    
    inline ::System::Data::DataTable^  Angle_sat_Data::satDataTable::CreateInstance() {
        return (gcnew UBX_parser::Angle_sat_Data::satDataTable());
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::InitVars() {
        this->columntime = __super::Columns[L"time"];
        this->columnsat_num = __super::Columns[L"sat_num"];
        this->columnsat_type = __super::Columns[L"sat_type"];
        this->columnazimuth = __super::Columns[L"azimuth"];
        this->columnelevation = __super::Columns[L"elevation"];
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::InitClass() {
        this->columntime = (gcnew ::System::Data::DataColumn(L"time", ::System::DateTime::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columntime);
        this->columnsat_num = (gcnew ::System::Data::DataColumn(L"sat_num", ::System::UInt32::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnsat_num);
        this->columnsat_type = (gcnew ::System::Data::DataColumn(L"sat_type", ::System::String::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnsat_type);
        this->columnazimuth = (gcnew ::System::Data::DataColumn(L"azimuth", ::System::Int64::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnazimuth);
        this->columnelevation = (gcnew ::System::Data::DataColumn(L"elevation", ::System::Int64::typeid, nullptr, ::System::Data::MappingType::Attribute));
        __super::Columns->Add(this->columnelevation);
        this->columntime->Namespace = L"";
        this->columnsat_num->Namespace = L"";
        this->columnsat_type->Namespace = L"";
        this->columnazimuth->Namespace = L"";
        this->columnelevation->Namespace = L"";
    }
    
    inline UBX_parser::Angle_sat_Data::satRow^  Angle_sat_Data::satDataTable::NewsatRow() {
        return (cli::safe_cast<UBX_parser::Angle_sat_Data::satRow^  >(this->NewRow()));
    }
    
    inline ::System::Data::DataRow^  Angle_sat_Data::satDataTable::NewRowFromBuilder(::System::Data::DataRowBuilder^  builder) {
        return (gcnew UBX_parser::Angle_sat_Data::satRow(builder));
    }
    
    inline ::System::Type^  Angle_sat_Data::satDataTable::GetRowType() {
        return UBX_parser::Angle_sat_Data::satRow::typeid;
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::OnRowChanged(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowChanged(e);
        {
            this->satRowChanged(this, (gcnew UBX_parser::Angle_sat_Data::satRowChangeEvent((cli::safe_cast<UBX_parser::Angle_sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::OnRowChanging(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowChanging(e);
        {
            this->satRowChanging(this, (gcnew UBX_parser::Angle_sat_Data::satRowChangeEvent((cli::safe_cast<UBX_parser::Angle_sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::OnRowDeleted(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowDeleted(e);
        {
            this->satRowDeleted(this, (gcnew UBX_parser::Angle_sat_Data::satRowChangeEvent((cli::safe_cast<UBX_parser::Angle_sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::OnRowDeleting(::System::Data::DataRowChangeEventArgs^  e) {
        __super::OnRowDeleting(e);
        {
            this->satRowDeleting(this, (gcnew UBX_parser::Angle_sat_Data::satRowChangeEvent((cli::safe_cast<UBX_parser::Angle_sat_Data::satRow^  >(e->Row)), 
                    e->Action)));
        }
    }
    
    inline ::System::Void Angle_sat_Data::satDataTable::RemovesatRow(UBX_parser::Angle_sat_Data::satRow^  row) {
        this->Rows->Remove(row);
    }
    
    inline ::System::Xml::Schema::XmlSchemaComplexType^  Angle_sat_Data::satDataTable::GetTypedTableSchema(::System::Xml::Schema::XmlSchemaSet^  xs) {
        ::System::Xml::Schema::XmlSchemaComplexType^  type = (gcnew ::System::Xml::Schema::XmlSchemaComplexType());
        ::System::Xml::Schema::XmlSchemaSequence^  sequence = (gcnew ::System::Xml::Schema::XmlSchemaSequence());
        UBX_parser::Angle_sat_Data^  ds = (gcnew UBX_parser::Angle_sat_Data());
        ::System::Xml::Schema::XmlSchemaAny^  any1 = (gcnew ::System::Xml::Schema::XmlSchemaAny());
        any1->Namespace = L"http://www.w3.org/2001/XMLSchema";
        any1->MinOccurs = ::System::Decimal(0);
        any1->MaxOccurs = ::System::Decimal::MaxValue;
        any1->ProcessContents = ::System::Xml::Schema::XmlSchemaContentProcessing::Lax;
        sequence->Items->Add(any1);
        ::System::Xml::Schema::XmlSchemaAny^  any2 = (gcnew ::System::Xml::Schema::XmlSchemaAny());
        any2->Namespace = L"urn:schemas-microsoft-com:xml-diffgram-v1";
        any2->MinOccurs = ::System::Decimal(1);
        any2->ProcessContents = ::System::Xml::Schema::XmlSchemaContentProcessing::Lax;
        sequence->Items->Add(any2);
        ::System::Xml::Schema::XmlSchemaAttribute^  attribute1 = (gcnew ::System::Xml::Schema::XmlSchemaAttribute());
        attribute1->Name = L"namespace";
        attribute1->FixedValue = ds->Namespace;
        type->Attributes->Add(attribute1);
        ::System::Xml::Schema::XmlSchemaAttribute^  attribute2 = (gcnew ::System::Xml::Schema::XmlSchemaAttribute());
        attribute2->Name = L"tableTypeName";
        attribute2->FixedValue = L"satDataTable";
        type->Attributes->Add(attribute2);
        type->Particle = sequence;
        ::System::Xml::Schema::XmlSchema^  dsSchema = ds->GetSchemaSerializable();
        if (xs->Contains(dsSchema->TargetNamespace)) {
            ::System::IO::MemoryStream^  s1 = (gcnew ::System::IO::MemoryStream());
            ::System::IO::MemoryStream^  s2 = (gcnew ::System::IO::MemoryStream());
            try {
                ::System::Xml::Schema::XmlSchema^  schema = nullptr;
                dsSchema->Write(s1);
                for (                ::System::Collections::IEnumerator^  schemas = xs->Schemas(dsSchema->TargetNamespace)->GetEnumerator(); schemas->MoveNext();                 ) {
                    schema = (cli::safe_cast<::System::Xml::Schema::XmlSchema^  >(schemas->Current));
                    s2->SetLength(0);
                    schema->Write(s2);
                    if (s1->Length == s2->Length) {
                        s1->Position = 0;
                        s2->Position = 0;
                        for (                        ; ((s1->Position != s1->Length) 
                                    && (s1->ReadByte() == s2->ReadByte()));                         ) {
                            ;
                        }
                        if (s1->Position == s1->Length) {
                            return type;
                        }
                    }
                }
            }
            finally {
                if (s1 != nullptr) {
                    s1->Close();
                }
                if (s2 != nullptr) {
                    s2->Close();
                }
            }
        }
        xs->Add(dsSchema);
        return type;
    }
    
    
    inline Angle_sat_Data::satRow::satRow(::System::Data::DataRowBuilder^  rb) : 
            ::System::Data::DataRow(rb) {
        this->tablesat = (cli::safe_cast<UBX_parser::Angle_sat_Data::satDataTable^  >(this->Table));
    }
    
    inline System::DateTime Angle_sat_Data::satRow::time::get() {
        try {
            return (cli::safe_cast<::System::DateTime >(this[this->tablesat->timeColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'time\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Angle_sat_Data::satRow::time::set(System::DateTime value) {
        this[this->tablesat->timeColumn] = value;
    }
    
    inline System::UInt32 Angle_sat_Data::satRow::sat_num::get() {
        try {
            return (cli::safe_cast<::System::UInt32 >(this[this->tablesat->sat_numColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'sat_num\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Angle_sat_Data::satRow::sat_num::set(System::UInt32 value) {
        this[this->tablesat->sat_numColumn] = value;
    }
    
    inline System::String^  Angle_sat_Data::satRow::sat_type::get() {
        try {
            return (cli::safe_cast<::System::String^  >(this[this->tablesat->sat_typeColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'sat_type\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Angle_sat_Data::satRow::sat_type::set(System::String^  value) {
        this[this->tablesat->sat_typeColumn] = value;
    }
    
    inline System::Int64 Angle_sat_Data::satRow::azimuth::get() {
        try {
            return (cli::safe_cast<::System::Int64 >(this[this->tablesat->azimuthColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'azimuth\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Angle_sat_Data::satRow::azimuth::set(System::Int64 value) {
        this[this->tablesat->azimuthColumn] = value;
    }
    
    inline System::Int64 Angle_sat_Data::satRow::elevation::get() {
        try {
            return (cli::safe_cast<::System::Int64 >(this[this->tablesat->elevationColumn]));
        }
        catch (::System::InvalidCastException^ e) {
            throw (gcnew ::System::Data::StrongTypingException(L"Значение для столбца \'elevation\' в таблице \'sat\' равно DBNull.", 
                e));
        }
    }
    inline System::Void Angle_sat_Data::satRow::elevation::set(System::Int64 value) {
        this[this->tablesat->elevationColumn] = value;
    }
    
    inline ::System::Boolean Angle_sat_Data::satRow::IstimeNull() {
        return this->IsNull(this->tablesat->timeColumn);
    }
    
    inline ::System::Void Angle_sat_Data::satRow::SettimeNull() {
        this[this->tablesat->timeColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Angle_sat_Data::satRow::Issat_numNull() {
        return this->IsNull(this->tablesat->sat_numColumn);
    }
    
    inline ::System::Void Angle_sat_Data::satRow::Setsat_numNull() {
        this[this->tablesat->sat_numColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Angle_sat_Data::satRow::Issat_typeNull() {
        return this->IsNull(this->tablesat->sat_typeColumn);
    }
    
    inline ::System::Void Angle_sat_Data::satRow::Setsat_typeNull() {
        this[this->tablesat->sat_typeColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Angle_sat_Data::satRow::IsazimuthNull() {
        return this->IsNull(this->tablesat->azimuthColumn);
    }
    
    inline ::System::Void Angle_sat_Data::satRow::SetazimuthNull() {
        this[this->tablesat->azimuthColumn] = ::System::Convert::DBNull;
    }
    
    inline ::System::Boolean Angle_sat_Data::satRow::IselevationNull() {
        return this->IsNull(this->tablesat->elevationColumn);
    }
    
    inline ::System::Void Angle_sat_Data::satRow::SetelevationNull() {
        this[this->tablesat->elevationColumn] = ::System::Convert::DBNull;
    }
    
    
    inline Angle_sat_Data::satRowChangeEvent::satRowChangeEvent(UBX_parser::Angle_sat_Data::satRow^  row, ::System::Data::DataRowAction action) {
        this->eventRow = row;
        this->eventAction = action;
    }
    
    inline UBX_parser::Angle_sat_Data::satRow^  Angle_sat_Data::satRowChangeEvent::Row::get() {
        return this->eventRow;
    }
    
    inline ::System::Data::DataRowAction Angle_sat_Data::satRowChangeEvent::Action::get() {
        return this->eventAction;
    }
}
