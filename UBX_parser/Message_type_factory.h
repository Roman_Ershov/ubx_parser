#pragma once
#include "Message_factory.h"
#include <unordered_map>
#include "Rxm_rawx_msg.h"
#include "Rxm_measx_msg.h"
#include "Nav_sat_msg.h"
#include "Aid_alm_msg.h"
#include "Aid_eph_msg.h"

namespace ublox
{
	class Message_type_factory
	{
		
		using typemap = std::unordered_map<UBX_MsgId, std::shared_ptr<Message_factory>>;
		typemap _factories;

	public:
		void register_all_factories()
		{
			_factories.insert({UBX_MsgId::RXM_RAWX, std::make_shared<Rxm_rawx_msg_factory>()});
			_factories.insert({UBX_MsgId::RXM_MEASX, std::make_shared<Rxm_measx_msg_factory>()});
			_factories.insert({UBX_MsgId::NAV_SAT, std::make_shared<Nav_sat_msg_factory>()});
			_factories.insert({ UBX_MsgId::AID_ALM, std::make_shared<Aid_alm_msg_factory>()});
			_factories.insert({ UBX_MsgId::AID_EPH, std::make_shared<Aid_eph_msg_factory>() });
		}

		std::shared_ptr<UBX_Message> create(UBX_MsgId msg_id,
											std::uint16_t length,
											std::uint16_t checksum,
											std::vector<std::uint8_t>&& data)
		{			
			try
			{
				return _factories.at(msg_id)->create(msg_id, length, checksum, std::forward<std::vector<std::uint8_t>>(data));
			}

			catch(std::exception &e)
			{
				return nullptr;
			}
		}
	};
}
