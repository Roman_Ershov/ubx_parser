#pragma once
#include <vector>
#include "UBX_Message.h"
#include "Message_factory.h"


namespace ublox
{

	struct Rxm_rawx_meas_data
	{
		double prMes;
		double cpMes;
		float doMes;
		std::uint8_t gnssId;
		std::uint8_t svId;
		std::uint8_t freqId;
		std::uint16_t lockTime;
		std::uint8_t cno;
		double prStdev;
		double cpStdev;
		double doStdev;
		bool prValid;
		bool cpValid;
		bool halfCyc;
		bool subHalfCyc;
	};

	struct Rxm_rawx_data
	{
		double rcvTow;
		std::uint16_t week;
		std::int8_t leapS;
		std::uint8_t numMeas;
		bool leapSec;
		bool clkReset;

		std::vector<Rxm_rawx_meas_data> meas_data;
	};

	class Rxm_rawx_msg : public UBX_Message
	{
		std::shared_ptr<Rxm_rawx_data> parsed_data;

	public:
		Rxm_rawx_msg(UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data);

		void do_parse() override;

		std::shared_ptr<Rxm_rawx_data> get_parsed_data();
	};


	class Rxm_rawx_msg_factory : public Message_factory
	{
		std::shared_ptr<UBX_Message> create(UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data) override;
	};

}

