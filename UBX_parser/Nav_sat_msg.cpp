#include "Nav_sat_msg.h"

ublox::Nav_sat_msg::Nav_sat_msg(UBX_MsgId msg_id,
								std::uint16_t length,
								std::uint16_t checksum,
								std::vector<std::uint8_t>&& data) :
								UBX_Message(msg_id, length, checksum, std::forward < std::vector<std::uint8_t> > (data))
{
	parsed_data = std::make_shared<Nav_sat_data>();
}

std::shared_ptr<ublox::Nav_sat_data> ublox::Nav_sat_msg::get_parsed_data()
{
	return  parsed_data;
}

std::shared_ptr<ublox::UBX_Message> ublox::Nav_sat_msg_factory::create(UBX_MsgId msg_id, std::uint16_t length, std::uint16_t checksum, std::vector<std::uint8_t>&& data)
{
	return std::make_shared<Nav_sat_msg>(msg_id, length, checksum, std::forward < std::vector<std::uint8_t> >(data));
}

void ublox::Nav_sat_msg::do_parse()
{
	size_t offset = 0;
	std::memcpy(&parsed_data->iTOW, data.data() + offset, sizeof(parsed_data->iTOW));
	offset += sizeof(parsed_data->iTOW);

	std::memcpy(&parsed_data->version, data.data() + offset, sizeof(parsed_data->version));
	offset += sizeof(parsed_data->version);

	std::memcpy(&parsed_data->numSvs, data.data() + offset, sizeof(parsed_data->numSvs));
	offset += sizeof(parsed_data->numSvs);

	offset += 2 * sizeof(std::uint8_t);

	for (auto numSv = 0; numSv < parsed_data->numSvs; numSv++) 
	{
		Nav_sat_sv_data sv_data;

		std::memcpy(&sv_data.gnssId, data.data() + offset, sizeof(sv_data.gnssId));
		offset += sizeof(sv_data.gnssId);

		std::memcpy(&sv_data.svId, data.data() + offset, sizeof(sv_data.svId));
		offset += sizeof(sv_data.svId);

		std::memcpy(&sv_data.cno, data.data() + offset, sizeof(sv_data.cno));
		offset += sizeof(sv_data.cno);

		std::memcpy(&sv_data.elev, data.data() + offset, sizeof(sv_data.elev));
		offset += sizeof(sv_data.elev);

		std::memcpy(&sv_data.azim, data.data() + offset, sizeof(sv_data.azim));
		offset += sizeof(sv_data.azim);

		std::int16_t prRes_;
		std::memcpy(&prRes_, data.data() + offset, sizeof(prRes_));
		offset += sizeof(prRes_);
		sv_data.prRes = 0.1 * prRes_;

		std::uint32_t bitmask_;
		std::memcpy(&bitmask_, data.data() + offset, sizeof(bitmask_));
		offset += sizeof(bitmask_);
		sv_data.qualityInd = bitmask_ & 0x07;
		sv_data.svUsed = bitmask_ & 0x08;
		sv_data.health = (bitmask_ & 0x20) >> 4;
		sv_data.diffCorr = bitmask_ & 0x40;
		sv_data.smoothed = bitmask_ & 0x80;
		sv_data.orbitSource = (bitmask_ >> 8) & 0x07;
		sv_data.ephAvail = bitmask_ & 0x800;
		sv_data.almAvail = bitmask_ & 0x1000;
		sv_data.anoAvail = bitmask_ & 0x2000;
		sv_data.aopAvail = bitmask_ & 0x4000;
		sv_data.sbasCorrUsed = bitmask_ & 0x10000;
		sv_data.rtcmCorrUsed = bitmask_ & 0x20000;
		sv_data.prCorrUsed = bitmask_ & 0x100000;
		sv_data.crCorrUsed = bitmask_ & 0x200000;
		sv_data.doCorrUsed = bitmask_ & 0x400000;

		parsed_data->sat_sv_data.emplace_back(sv_data);
	}
}