#include <fstream>
#include <vector>
#include "UBX_Parser.h"
using namespace std;

void main()
{
	/*char path[] = "COM34_181106_131711.ubx";
	ifstream ubx_file;
	ubx_file.open(path, ios::binary | ios::in);

	vector <unsigned char> data;
	data.assign(istreambuf_iterator<char>(ubx_file), istreambuf_iterator<char>());
	ubx_file.close();

	ofstream out("data.txt", ios::out);
	for (unsigned long pos = 0; pos < data.size(); pos++)
	{
		out << hex << (int)data[pos] << " ";
	}
	out << endl;
	out.close();*/

	std::unique_ptr<UBX_Parser> ubx_parser = std::make_unique<UBX_Parser>("COM1_190206_094158.ubx");
	ubx_parser->do_parse();
	ubx_parser->export_to_xml();
	ubx_parser->export_to_xml_peleng();
}