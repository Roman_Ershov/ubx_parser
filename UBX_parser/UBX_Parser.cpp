#include "UBX_Parser.h"
#include "Message_type_factory.h"
#include "Sat_data_format.hxx"
#include "Angle_sat_data_format.hxx"
#include <chrono>

void UBX_Parser::do_parse()
{
	std::istreambuf_iterator<uint8_t> istreambuf_iterator(ubx_file);
	const std::istreambuf_iterator<uint8_t> eof;

	std::unique_ptr<ublox::Message_type_factory> message_type_factory = std::make_unique<ublox::Message_type_factory>();
	message_type_factory->register_all_factories();

	while (istreambuf_iterator != eof)
	{
		if (*(istreambuf_iterator++) == 0xb5)
		{
			if (*(istreambuf_iterator++) == 0x62)
			{
				auto msg_id = static_cast<ublox::UBX_MsgId>((*(istreambuf_iterator++) << 8) | *(istreambuf_iterator++));
				auto length_1 = *(istreambuf_iterator++);
				auto length_2 = *(istreambuf_iterator++);
				auto msg_len = to_big_endian(length_1, length_2);

				std::vector <uint8_t> msg_data;
				for (size_t ctr = 0; ctr < msg_len; ctr++)
				{
					msg_data.emplace_back(*(istreambuf_iterator++));
				}

				uint16_t checksum = (*(istreambuf_iterator++) << 8) | *(istreambuf_iterator++);
				auto msg = message_type_factory->create(msg_id, msg_len, checksum, std::move(msg_data));

				if (msg != nullptr) {
					msg->do_parse();
					messages.emplace_back(msg);
				}
			}
		}

		++istreambuf_iterator;
	}
}

void UBX_Parser::export_to_xml()
{
	try
	{
		satns::Sat_Data::sat_sequence sat_sequence;

		auto find_first = std::find_if(messages.begin(), messages.end(),
			[](std::shared_ptr<ublox::UBX_Message> msg)
		{
			return msg->getType() == ublox::UBX_MsgId::RXM_MEASX;
		});

		auto message_find = *find_first;

		std::shared_ptr<ublox::Rxm_measx_msg> measx_msg =
			std::dynamic_pointer_cast<ublox::Rxm_measx_msg>(message_find);
		std::shared_ptr<ublox::Rxm_measx_data> measx_data = measx_msg->get_parsed_data();

		find_first = std::find_if(find_first, messages.end(),
			[](std::shared_ptr<ublox::UBX_Message> msg)
		{
			return msg->getType() == ublox::UBX_MsgId::RXM_RAWX;
		});
		std::shared_ptr<ublox::Rxm_rawx_msg> rawx_msg =
			std::dynamic_pointer_cast<ublox::Rxm_rawx_msg>(*find_first);
		std::shared_ptr<ublox::Rxm_rawx_data> rawx_data = rawx_msg->get_parsed_data();

		std::tm tm = { 0 };
		tm.tm_mday = 6;
		tm.tm_mon = 0;
		tm.tm_year = 80;
		std::chrono::time_point<std::chrono::system_clock> tp_1980 =
			std::chrono::system_clock::from_time_t(std::mktime(&tm));
		tp_1980 += std::chrono::seconds(rawx_data->week * 7 * 24 * 3600 +
			static_cast<uint64_t>(rawx_data->rcvTow) - rawx_data->leapS);
		time_t tt = std::chrono::system_clock::to_time_t(tp_1980);
		std::tm utc_tm = { 0 };
		localtime_s(&utc_tm, &tt);

		xml_schema::date_time date_time(utc_tm.tm_year + 1900,
			utc_tm.tm_mon + 1,
			utc_tm.tm_mday,
			utc_tm.tm_hour,
			utc_tm.tm_min,
			utc_tm.tm_sec);

		for (size_t sat_num = 0; sat_num < rawx_data->numMeas; sat_num++)
		{
			satns::SatType sat;
			if (rawx_data->meas_data[sat_num].gnssId > 0 && rawx_data->meas_data[sat_num].gnssId < 6)
				continue;
			sat.time(date_time);
			sat.sat_num(rawx_data->meas_data[sat_num].svId);
			sat.sat_type((rawx_data->meas_data[sat_num].gnssId == 0) ? satns::gnss_type("GPS") : 
				satns::gnss_type("GLONASS"));
			sat.pseudorange(rawx_data->meas_data[sat_num].prMes);

			auto sat_measx_data = std::find_if(measx_data->sat_data.begin(), measx_data->sat_data.end(),
				[rawx_data, sat_num](ublox::Rxm_measx_sat_data sat_data)
			{
				return (sat_data.svId == rawx_data->meas_data[sat_num].svId) && 
					(sat_data.gnssId == rawx_data->meas_data[sat_num].gnssId);
			});

			sat.radial_velocity((*sat_measx_data).dopplesMS);
			sat.doppler_shift(rawx_data->meas_data[sat_num].doMes);
			sat.pseudorange_sqr(rawx_data->meas_data[sat_num].prStdev);
			sat.doppler_sqr(rawx_data->meas_data[sat_num].doStdev);
			
			sat_sequence.push_back(sat);
		}
								
		std::shared_ptr<satns::Sat_Data> sat_data = std::make_shared<satns::Sat_Data>();
		sat_data->sat(sat_sequence);

		xml_schema::namespace_infomap map;
		map[""].name = "urn:satns";
		map[""].schema = "Sat_data_format.xsd";
		std::ofstream ofs("Sat_data.xml");
		Sat_Data_(ofs, *sat_data, map);
	}
	catch(const xml_schema::exception &e)
	{
		
	}
}

void UBX_Parser::export_to_xml_peleng()
{
	try
	{
		angle_sat::Angle_sat_Data::sat_sequence sat_sequence;
		auto find_first = std::find_if(messages.begin(), messages.end(),
			[](std::shared_ptr<ublox::UBX_Message> msg)
		{
			return msg->getType() == ublox::UBX_MsgId::NAV_SAT;
		});

		auto message_find = *find_first;

		std::shared_ptr<ublox::Nav_sat_msg> nav_sat_msg =
			std::dynamic_pointer_cast<ublox::Nav_sat_msg>(message_find);
		std::shared_ptr<ublox::Nav_sat_data> nav_sat_data = nav_sat_msg->get_parsed_data();

		
		std::tm tm = { 0 };
		tm.tm_mday = 6;
		tm.tm_mon = 0;
		tm.tm_year = 80;
		std::chrono::time_point<std::chrono::system_clock> tp_1980 =
			std::chrono::system_clock::from_time_t(std::mktime(&tm));
		tp_1980 += std::chrono::seconds(2026 * 7 * 24 * 3600 +
			static_cast<uint64_t>(nav_sat_data->iTOW/1000) - 18);
		time_t tt = std::chrono::system_clock::to_time_t(tp_1980);
		std::tm utc_tm = { 0 };
		localtime_s(&utc_tm, &tt);

		xml_schema::date_time date_time(utc_tm.tm_year + 1900,
			utc_tm.tm_mon + 1,
			utc_tm.tm_mday,
			utc_tm.tm_hour,
			utc_tm.tm_min,
			utc_tm.tm_sec);

		for (size_t sat_num = 0; sat_num < nav_sat_data->numSvs; sat_num++)
		{
			angle_sat::AngleType sat;
			if (nav_sat_data->sat_sv_data[sat_num].gnssId > 0 && nav_sat_data->sat_sv_data[sat_num].gnssId < 6)
				continue;
			sat.time(date_time);
			sat.sat_num(nav_sat_data->sat_sv_data[sat_num].svId);
			sat.sat_type((nav_sat_data->sat_sv_data[sat_num].gnssId == 0) ? angle_sat::gnss_type("GPS") : angle_sat::gnss_type("GLONASS"));
			sat.azimuth(nav_sat_data->sat_sv_data[sat_num].azim);
			sat.elevation(nav_sat_data->sat_sv_data[sat_num].elev);

			sat_sequence.push_back(sat);
		}

		std::shared_ptr<angle_sat::Angle_sat_Data> sat_data = std::make_shared<angle_sat::Angle_sat_Data>();
		sat_data->sat(sat_sequence);

		xml_schema::namespace_infomap map;
		map[""].name = "urn:angle_sat";
		map[""].schema = "Angle_sat_data_format.xsd";
		std::ofstream ofs("Angle_sat_data.xml");
		angle_sat::Angle_sat_Data_(ofs, *sat_data, map);
	}
	catch (const xml_schema::exception &e)
	{

	}
}

uint16_t UBX_Parser::to_big_endian(uint8_t byte_1, uint8_t byte_2)
{
	return (byte_2 << 8) | byte_1;
}

UBX_Parser::UBX_Parser(const std::string& file)
{
	ubx_file.open(file.c_str(), std::ios::binary);
}

UBX_Parser::~UBX_Parser()
{
	ubx_file.close();
}
