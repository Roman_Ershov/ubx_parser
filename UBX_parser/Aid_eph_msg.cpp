#include "Aid_eph_msg.h"

ublox::Aid_eph_msg::Aid_eph_msg(UBX_MsgId msg_id,
	std::uint16_t length,
	std::uint16_t checksum,
	std::vector<std::uint8_t>&& data) :

	UBX_Message(msg_id, length, checksum, std::forward<std::vector<std::uint8_t>>(data))
{
	//parsed_data = std::make_shared<Rxm_rawx_data>();
}

std::shared_ptr<ublox::UBX_Message> ublox::Aid_eph_msg_factory::create(UBX_MsgId msg_id,
	std::uint16_t length,
	std::uint16_t checksum,
	std::vector<std::uint8_t>&& data)
{
	return std::make_shared<Aid_eph_msg>(msg_id, length, checksum, std::forward<std::vector<std::uint8_t>>(data));
}

void ublox::Aid_eph_msg::do_parse()
{}
