#pragma once
#include <memory>

namespace ublox
{
	class Message_factory
	{
	public:
		virtual std::shared_ptr<UBX_Message> create(	UBX_MsgId msg_id,
														std::uint16_t length,
														std::uint16_t checksum,
														std::vector<std::uint8_t>&& data) = 0;
		virtual ~Message_factory() = default;
	};

}
