#pragma once

#include "UBX_MsgId.h"
#include <vector>
#include <memory>

namespace ublox
{
	
	class UBX_Message
	{
	protected:
		UBX_MsgId msg_id_;
		std::uint16_t length;
		std::uint16_t checksum;
		std::vector <std::uint8_t> data;

	public:

		UBX_Message(ublox::UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data) :
			msg_id_(msg_id),
			length(length),
			checksum(checksum),
			data(data)
		{}

		UBX_MsgId getType()
		{
			return msg_id_;
		}

		UBX_Message() = default;
		UBX_Message(const UBX_Message&) = default;
		UBX_Message(UBX_Message&&) = default;
		UBX_Message& operator=(const UBX_Message&) = default;
		UBX_Message& operator=(UBX_Message&&) = default;

		virtual ~UBX_Message() = default;
		virtual void do_parse() = 0; 
	};
}


