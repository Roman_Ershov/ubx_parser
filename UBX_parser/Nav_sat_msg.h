#pragma once
#include "UBX_Message.h"
#include "Message_factory.h"

namespace  ublox
{
	struct Nav_sat_sv_data
	{
		std::uint8_t gnssId;
		std::uint8_t svId;
		std::uint8_t cno;
		std::int8_t elev;
		std::int16_t azim;
		double prRes;

		std::uint8_t qualityInd;
		bool svUsed;
		std::uint8_t health;
		bool diffCorr;
		bool smoothed;
		std::uint8_t orbitSource;
		bool ephAvail;
		bool almAvail;
		bool anoAvail;
		bool aopAvail;
		bool sbasCorrUsed;
		bool rtcmCorrUsed;
		bool slasCorrUsed;
		bool prCorrUsed;
		bool crCorrUsed;
		bool doCorrUsed;
	};

	struct Nav_sat_data
	{
		std::uint32_t iTOW;
		std::uint8_t version;
		std::uint8_t numSvs;

		std::vector<Nav_sat_sv_data> sat_sv_data;
	};

	class Nav_sat_msg : public UBX_Message
	{
		std::shared_ptr<Nav_sat_data> parsed_data;

	public:
		void do_parse() override;
		Nav_sat_msg(UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data);

		std::shared_ptr<Nav_sat_data> get_parsed_data();
	};

	class Nav_sat_msg_factory : public Message_factory
	{
	public:
		std::shared_ptr<UBX_Message> create(UBX_MsgId msg_id,
											std::uint16_t length,
											std::uint16_t checksum,
											std::vector<std::uint8_t>&& data) override;
	};
}
