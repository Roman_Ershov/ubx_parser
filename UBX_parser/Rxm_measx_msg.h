#pragma once
#include "UBX_Message.h"
#include "Message_factory.h"

namespace ublox
{
	struct Rxm_measx_sat_data
	{
		std::uint8_t gnssId;
		std::uint8_t svId;
		std::uint8_t cNo;
		std::uint8_t mpathIndic;
		double dopplesMS;
		double dopplerHz;
		std::uint16_t wholeChips;
		std::uint16_t fracChips;
		double codePhase;
		std::uint8_t intCodePhase;
		std::uint8_t pseuRangeRMESrr;
	};

	struct Rxm_measx_data
	{
		std::uint8_t version;
		std::uint32_t gpsTOW;
		std::uint32_t gloTOW;
		std::uint32_t bdsTOW;
		std::uint32_t qzssTOW;
		double gpsTOWAcc;
		double gloTOWAcc;
		double bdsTOWAcc;
		double qzssTOWAcc;
		std::uint8_t numSV;
		bool towSet;

		std::vector<Rxm_measx_sat_data> sat_data;
	};

	class Rxm_measx_msg : public UBX_Message
	{
		std::shared_ptr<Rxm_measx_data> parsed_data;

	public:
		Rxm_measx_msg(UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data);

		void do_parse() override;

		std::shared_ptr<Rxm_measx_data> get_parsed_data();
	};

	class Rxm_measx_msg_factory: public Message_factory
	{
		std::shared_ptr<UBX_Message> create(UBX_MsgId msg_id,
			std::uint16_t length,
			std::uint16_t checksum,
			std::vector<std::uint8_t>&& data) override;
	};

	
}
